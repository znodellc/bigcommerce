﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace ArtifiConnector.Api
{
    public class CartController : ApiController
    {
        #region Injected Properties

        [Inject]
        public IArtifiWebClient ArtifiWebClient { get; set; }

        [Inject]
        public ICartRepository CartRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        #endregion InjectedProperties

        [HttpPost]
        [Route("api/cart")]
        public async Task<IHttpActionResult> AddCart()
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var content = await Request.Content.ReadAsStringAsync();
                var cartItem = JsonConvert.DeserializeObject<CartItem>(content);

                if (cartItem == null)
                    return BadRequest();

                CartRepository.Add(cartItem, merchantId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpDelete]
        [Route("api/cart")]
        public async Task<IHttpActionResult> DeleteCart()
        {
            try
            {
                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);
                var cartItemId = jobject.Value<string>("cartItemId");

                var designId = CartRepository.GetDesignId(cartItemId);

                if (!designId.HasValue)
                    return Ok();

                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var merchant = MerchantRepository.Get(merchantId);

                await ArtifiWebClient.RemoveCartItem(designId.Value, merchant.WebsiteId, merchant.WebApiClientKey);

                CartRepository.Remove(cartItemId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/cart/{cartItemId}/designId")]
        public IHttpActionResult GetDesignId(string cartItemId)
        {
            try
            {
                var result = CartRepository.GetDesignId(cartItemId);

                if (result == null)
                    return NotFound();

                return Ok(new { designId = result });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/cart/order/{orderItemId}/designId")]
        public IHttpActionResult GetDesignIdByOrderItemId(int orderItemId)
        {
            try
            {
                var result = CartRepository.GetDesignIdByOrderItemId(orderItemId);

                if (result == null)
                    return NotFound();

                return Ok(new { designId = result });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/cart/{cartItemId}/previewUrl")]
        public IHttpActionResult GetPreviewUrl(string cartItemId)
        {
            try
            {
                var result = CartRepository.GetPreviewUrl(cartItemId);

                if (string.IsNullOrWhiteSpace(result))
                    return NotFound();

                return Ok(new { previewUrl = result });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/cart/customer/{customerId}/product/{productId}/previewUrl")]
        public IHttpActionResult GetPreviewUrl(int customerId, int productId)
        {
            try
            {
                var result = CartRepository.GetPreviewUrl(customerId, productId);

                if (string.IsNullOrWhiteSpace(result))
                    return NotFound();

                return Ok(new { previewUrl = result });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/cart/order/{orderItemId}/previewUrl")]
        public IHttpActionResult GetPreviewUrlByOrderItemId(int orderItemId)
        {
            try
            {
                var result = CartRepository.GetPreviewUrlByOrderItemId(orderItemId);

                if (string.IsNullOrWhiteSpace(result))
                    return NotFound();

                return Ok(new { previewUrl = result });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpPut]
        [Route("api/cart")]
        public async Task<IHttpActionResult> UpdateCart()
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);

                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);

                var previewUrl = jobject.Value<string>("previewUrl");
                var designId = jobject.Value<int>("designId");
                var customerId = jobject.Value<int?>("customerId");

                CartRepository.Update(designId, previewUrl, customerId, merchantId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }
    }
}