﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.Api;
using ArtifiConnector.Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Routing;

namespace ArtifiConnector.Tests.Api
{
    [TestClass]
    public class ProductsControllerTests
    {
        private ProductsController _controller;
        private Mock<HttpRequestMessage> _request;
        private Mock<IMerchantRepository> _mockMerchantRepository;
        private Mock<IProductRepository> _mockProductRepository;
        private Mock<IConfigurationRepository> _mockConfigurationRepository;

        [TestInitialize]
        public void Initialize()
        {
            _mockProductRepository = new Mock<IProductRepository>();
            _mockMerchantRepository = new Mock<IMerchantRepository>();
            _mockConfigurationRepository = new Mock<IConfigurationRepository>();
            _request = new Mock<HttpRequestMessage>();
            _controller = new ProductsController();
            _controller.ControllerContext = new HttpControllerContext();
            _controller.Request = _request.Object;
            _controller.MerchantRepository = _mockMerchantRepository.Object;
            _controller.ProductRepository = _mockProductRepository.Object;
            _controller.ConfigurationRepository = _mockConfigurationRepository.Object;
        }

        [TestMethod]
        public void ProductsController_IsCustomized_CustomizedProductNoDesignConfiguration()
        {
            _mockMerchantRepository.Setup(x => x.GetMerchantIdByHash("test")).Returns(123);
            _mockProductRepository.Setup(x => x.GetProduct(1, 123)).Returns(new Product { Sku = "test_sku" });
            _request.Object.Headers.Authorization = new AuthenticationHeaderValue("test");
            var result = _controller.IsCustomized(1) as OkNegotiatedContentResult<IsCustomizedResponse>;

            Assert.AreEqual(1, result.Content.Result);
            Assert.AreEqual("test_sku", result.Content.ProductSku);
        }

        [TestMethod]
        public void ProductsController_IsCustomized_CustomizedProductWithDesignConfiguration()
        {
            _mockMerchantRepository.Setup(x => x.GetMerchantIdByHash("test")).Returns(123);
            _mockProductRepository.Setup(x => x.GetProduct(1, 123)).Returns(new Product { Sku = "test_sku" });
            _mockConfigurationRepository.Setup(x => x.GetDesignConfiguration(123)).Returns(new List<DesignConfiguration> { new DesignConfiguration { Key = "CustomizedButtonLabel", Value = "123" } });
            _request.Object.Headers.Authorization = new AuthenticationHeaderValue("test");
            var result = _controller.IsCustomized(1) as OkNegotiatedContentResult<IsCustomizedResponse>;

            Assert.AreEqual(1, result.Content.Result);
            Assert.AreEqual("test_sku", result.Content.ProductSku);
            Assert.AreEqual("123", result.Content.CustomizedButtonLabel);
        }

        [TestMethod]
        public void ProductsController_IsCustomized_ProductNotFound()
        {
            _request.Object.Headers.Authorization = new AuthenticationHeaderValue("test");
            var result = _controller.IsCustomized(1) as OkNegotiatedContentResult<IsCustomizedResponse>;

            Assert.AreEqual(0, result.Content.Result);
        }
    }
}
