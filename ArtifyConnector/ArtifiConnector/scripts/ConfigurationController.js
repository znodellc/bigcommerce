﻿var configurationController = function () {
    return {

        showSuccessBox: function (msg) {
            $("#successBox").css('display', 'block');
            $('#successBox').fadeIn().delay(1500).fadeOut(1000);
            $('#successBox').text(msg);
        },
        showFailBox: function (msg) {
            $("#failBox").css('display', 'block');
            $('#failBox').fadeIn().delay(1500).fadeOut(1000);
            $('#failBox').text(msg);
        }
    }
}();

$.fn.serializeDesignConfiguration = function () {
    var o = {};
    var a = this.serializeArray();

    var result = [];

    for (var i = 0; i < a.length; i+=2)
    {
        var json = { id: a[i].value, value: a[i + 1].value };
        result.push(json);
    }

    return result;
};

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};