﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtifiConnector.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace ArtifiConnector.Data.Dapper
{
    public class CartRepository : ICartRepository
    {
        private readonly string _connectionString;

        public CartRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(CartItem item, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("spInsertCartItem", commandType: CommandType.StoredProcedure, param: new
                {
                    ProductId = item.ProductId,
                    CartItemId = item.CartItemId,
                    DesignId = item.DesignId,
                    PreviewUrl = item.PreviewUrl,
                    CustomerId = item.CustomerId,
                    MerchantId = merchantId
                });
            }
        }

        public void Remove(string cartItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("DELETE FROM CartItem Where CartItemId = @CartItemId", new { CartItemId = cartItemId });
            }
        }

        public int? GetDesignId(string cartItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<int?>("SELECT DesignId FROM CartItem Where CartItemId = @CartItemId", new { CartItemId = cartItemId }).FirstOrDefault();
            }
        }

        public int? GetDesignIdByOrderItemId(int orderItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<int?>("SELECT DesignId FROM CartItem Where OrderItemId = @OrderItemId", new { OrderItemId = orderItemId }).FirstOrDefault();
            }
        }

        public string GetPreviewUrl(string cartItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT PreviewUrl FROM CartItem Where CartItemId = @CartItemId", new { CartItemId = cartItemId }).FirstOrDefault();
            }
        }

        public string GetPreviewUrl(int customerId, int productId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT PreviewUrl FROM CartItem Where CustomerId = @CustomerId and ProductId = @ProductId", new { CustomerId = customerId, ProductId = productId }).FirstOrDefault();
            }
        }

        public string GetPreviewUrlByOrderItemId(int orderItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT PreviewUrl FROM CartItem Where OrderItemId = @OrderItemId", new { OrderItemId = orderItemId }).FirstOrDefault();
            }
        }

        public CartItem Get(int productId, int merchantId, int customerId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<CartItem>("spGetCartItem", commandType: CommandType.StoredProcedure, param: new { ProductId = productId, MerchantId = merchantId, CustomerId = customerId }).FirstOrDefault();
            }
        }

        public void UpdateOrderItemId(int designId, int orderItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE CartItem SET OrderItemId = @OrderItemId Where DesignId = @DesignId", new { OrderItemId = orderItemId, DesignId = designId });
            }
        }

        public void Update(int designId, string previewUrl, int? customerId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("spUpdateCartItem", commandType: CommandType.StoredProcedure, param: new { DesignId = designId, PreviewUrl = previewUrl, CustomerId = customerId, MerchantId = merchantId });
            }
        }
    }
}
