﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.Extensions;
using ArtifiConnector.Models;
using ArtifiConnector.Properties;
using ArtifiConnector.ViewModels;
using ArtifiConnector.WebClients;
using ArtifiConnector.WebClients.Models;
using Ninject;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace ArtifiConnector.Controllers
{
    public class BigCommerceController : Controller
    {
        #region Injected Properties

        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public IConfigurationRepository ConfigurationRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        #endregion Injected Properties

        public ActionResult ApplicationConfiguration()
        {
            try
            {
                var merchantId = int.Parse(Session["MerchantId"].ToString());
                var configuration = ConfigurationRepository.GetApplicationConfiguration(merchantId);

                return View("ApplicationConfiguration", configuration);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public async Task<ActionResult> Auth([FromBody] AuthRequest request)
        {
            try
            {
                var receiveTokenRequest = new ReceiveTokenRequest
                {
                    ClientId = Settings.Default.ClientId,
                    ClientSecret = Settings.Default.ClientSecret,
                    Code = request.Code,
                    Scope = request.Scope,
                    RedirectUri = this.GetRequestUrl(),
                    Context = request.Context
                };

                var result = await BigCommerceWebClient.ReceiveTokenAsync(receiveTokenRequest);
                var merchant = MerchantRepository.Get(result.User.Id);

                if (merchant == null)
                {
                    var hash = GenerateHash();
                    var storeHash = result.Context.Split('/').Last();
                    merchant = new Merchant { Id = result.User.Id, AccessToken = result.AccessToken, StoreHash = storeHash, Hash = GenerateHash() };
                    MerchantRepository.Add(merchant);
                }
                else
                {
                    MerchantRepository.UpdateAccessToken(merchant.Id, result.AccessToken);
                }

                Session["MerchantId"] = merchant.Id;

                return ShowInstallationView(merchant.Id);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult Dashboard()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult DesignConfiguration()
        {
            try
            {
                var merchantId = int.Parse(Session["MerchantId"].ToString());

                var designConfiguration = ConfigurationRepository.GetDesignConfiguration(merchantId);

                return View("DesignConfiguration", designConfiguration);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult Load()
        {
            try
            {
                if (Request.QueryString.Count != 1)
                    throw new InvalidOperationException();

                var signedPayload = Request.QueryString[0];
                var store = BigCommerceDataProvider.ReadBigCommerceData(signedPayload, Settings.Default.ClientSecret);

                var merchant = MerchantRepository.Get(store.User.Id);

                if (merchant == null)
                    throw new InvalidOperationException();

                Session["MerchantId"] = merchant.Id;

                if (merchant.IsConfigured)
                    return View("Dashboard");
                else
                {
                    return ShowInstallationView(merchant.Id);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult ProductsConfiguration(string keyword = null)
        {
            try
            {
                var merchantId = int.Parse(Session["MerchantId"].ToString());
                var configuration = ConfigurationRepository.GetDesignConfiguration(merchantId);

                var label = configuration.FirstOrDefault(x => x.Key == "CustomizedButtonLabel")?.Value;

                var viewModel = new ProductsConfigurationViewModel
                {
                    ButtonLabel = label ?? "Customizable",
                    Keyword = keyword
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult SetConfigured()
        {
            try
            {
                var merchantId = int.Parse(Session["MerchantId"].ToString());
                MerchantRepository.Configure(merchantId);
                return ApplicationConfiguration();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        public ActionResult Uninstall()
        {
            try
            {
                if (Request.QueryString.Count != 1)
                    throw new InvalidOperationException();

                var signedPayload = Request.QueryString[0];
                var store = BigCommerceDataProvider.ReadBigCommerceData(signedPayload, Settings.Default.ClientSecret);

                MerchantRepository.Remove(store.User.Id);
                return View();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }

        private string GenerateHash()
        {
            var input = Guid.NewGuid().ToString();
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        private ActionResult ShowInstallationView(int merchantId)
        {
            try
            {
                var hash = MerchantRepository.GetMerchantHashById(merchantId);
                var scheme = Request.Url.Scheme;
                var authority = Request.Url.Authority;
                var url = Url.Content($"~/library/script/{hash}/bigcommerce.artifi.connector.js");
                var path = $"{scheme}://{authority}{url}";

                var viewModel = new InstallationViewModel
                {
                    JavaScriptLibraryLink = path
                };

                return View("Installation", viewModel);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }
    }
}