[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ArtifiConnector.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ArtifiConnector.App_Start.NinjectWebCommon), "Stop")]

namespace ArtifiConnector.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using WebClients;
    using Ninject.Web.WebApi;
    using System.Web.Http;
    using ArtifiConnector.Data;
    using System.Configuration;
    using ArtifiConnector.Data.Dapper;
    using Properties;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);

                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ArtifiConnectorConnectionString"].ConnectionString;

            kernel.Bind<IBigCommerceWebClient>().To<BigCommerceWebClient>().WithConstructorArgument(Settings.Default.ClientId);
            kernel.Bind<IArtifiWebClient>().To<ArtifiWebClient>().WithConstructorArgument(Settings.Default.ClientId);

            kernel.Bind<IProductRepository>().To<ProductRepository>().WithConstructorArgument(connectionString);
            kernel.Bind<ICartRepository>().To<CartRepository>().WithConstructorArgument(connectionString);
            kernel.Bind<IMerchantRepository>().To<MerchantRepository>().WithConstructorArgument(connectionString);
            kernel.Bind<IConfigurationRepository>().To<ConfigurationRepository>().WithConstructorArgument(connectionString);
            kernel.Bind<ICustomerRepository>().To<CustomerRepository>().WithConstructorArgument(connectionString);

            kernel.Bind<ILogger>().To<Logger>();

            kernel.Bind<OptionsManager>().ToSelf();
        }
    }
}
