﻿using ArtifiConnector.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data
{
    public interface ICustomerRepository
    {
        int AddGuest(int merchantId, string email);
        int Add(Customer customer);

        bool CustomerEmailExists(string email);
        Customer GetCustomerByEmail(string email);
        void UpdateCustomer(Customer customer);
    }
}
