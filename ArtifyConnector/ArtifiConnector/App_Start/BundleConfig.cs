﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ArtifiConnector.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var bootstrapCdn = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js";

            bundles.Add(new ScriptBundle("~/bundles/bootstrap", bootstrapCdn).Include(
                "~/scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/artifi").Include(
                "~/scripts/main.js",
                "~/scripts/bigcommerce.artifi.connector.js"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables",
                "https://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"));
            bundles.Add(new ScriptBundle("~/bundles/dataTablesBootstrap",
                "https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/configurationController").Include(
             "~/scripts/configurationController.js"));

            bundles.Add(new StyleBundle("~/content/main").Include(
               "~/content/main.css"));

            bundles.Add(new StyleBundle("~/content/normalize").Include(
               "~/content/normalize.css"));

            bundles.Add(new StyleBundle("~/content/bootstrap",
                "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css").Include(
                "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/content/dataTables",
                "https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.css"));

            bundles.Add(new StyleBundle("~/content/dataTablesArtifi").Include(
                "~/content/dataTablesArtifi.css"));

            BundleTable.EnableOptimizations = true;
            bundles.UseCdn = true;
        }
    }
}