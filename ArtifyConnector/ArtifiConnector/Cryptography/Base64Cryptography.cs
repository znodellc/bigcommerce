﻿using System;
using System.Text;

namespace ArtifiConnector.Cryptography
{
    public static class Base64Cryptography
    {
        public static string Decode(string base64string)
        {
            var bytes = Convert.FromBase64String(base64string);
            return Encoding.UTF8.GetString(bytes);
        }
    }
}