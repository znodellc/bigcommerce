﻿using System;
using System.Diagnostics;

namespace ArtifiConnector
{
    public class Logger : ILogger
    {
        private TraceSource _source;

        public Logger()
        {
            _source = new TraceSource("ArtifiConnectorSource");
        }

        public void LogError(string message)
        {
            try
            {
                _source.TraceEvent(TraceEventType.Error, 1, message);
            }
            catch (Exception)
            {
            }
        }

        public void LogInformation(string message)
        {
            try
            {
                _source.TraceEvent(TraceEventType.Information, 3, message);
            }
            catch (Exception)
            {
            }
        }

        public void LogWarning(string message)
        {
            try
            {
                _source.TraceEvent(TraceEventType.Warning, 2, message);
            }
            catch (Exception)
            {
            }
        }
    }
}