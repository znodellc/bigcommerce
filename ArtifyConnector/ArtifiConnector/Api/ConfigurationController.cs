﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ArtifiConnector.Api
{
    public class ConfigurationController : ApiController
    {
        [Inject]
        public IConfigurationRepository ConfigurationRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [HttpPost]
        [Route("api/configuration/application")]
        public async Task<IHttpActionResult> SaveApplicationConfiguration()
        {
            try
            {
                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);
                var websiteId = jobject.Value<string>("WebsiteId");
                var webApiClientKey = jobject.Value<string>("WebApiClientKey");
                var merchantId = jobject.Value<int>("MerchantId");
                ConfigurationRepository.SaveApplicationConfiguration(websiteId, webApiClientKey, merchantId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/configuration/design")]
        public async Task<IHttpActionResult> SaveDesignConfiguration()
        {
            try
            {
                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);
                var array = jobject.Value<JArray>("array");
                var configuration = array.Select(x => new DesignConfiguration { Id = x.Value<int>("id"), Value = x.Value<string>("value") });
                ConfigurationRepository.SaveDesignConfiguration(configuration);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError();
            }
        }
    }
}