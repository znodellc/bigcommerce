﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using ArtifiConnector.Tests.Properties;

namespace ArtifiConnector.Tests
{
    [TestClass]
    public class StoreProviderTests
    {
        [TestMethod]
        public void SignedPayloadReader_ReadStore_Success()
        {
            var signedPayload = "eyJ1c2VyIjp7ImlkIjoxLCJlbWFpbCI6InRlc3RAdGVzdC5jb20ifSwib3duZXIiOnsiaWQiOjEsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSJ9LCJjb250ZXh0Ijoic3RvcmVzIC8gdGVzdCIsInN0b3JlX2hhc2giOiJ0ZXN0IiwidGltZXN0YW1wIjoxNDc3NjQ5NzU3LjYyMzM0MTh9.MUIxQkM4QUJEMzJGREU2N0VBN0IyQkMzRkQ0MkM4QkM2RTE2M0FEQUE4N0NCNzZCNTUwMzRCRjUxRjQ4QTNFQg==";
            var clientSecret = "test_secret";
            var store = BigCommerceDataProvider.ReadBigCommerceData(signedPayload, clientSecret);

            Assert.IsNotNull(store);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SignedPayloadReader_ReadStore_InvalidSignedPayload()
        {
            var signedPayload = "eyJ1c2VyIjp7ImlkIjoxLCJlbWFpbCI6InRlc3RAdGVzdC5jb20ifSwib3duZXIiOnsiaWQiOjEsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSJ9LCJjb250ZXh0Ijoic3RvcmVzIC8gdGVzdCIsInN0b3JlX2hhc2giOiJ0ZXN0IiwidGltZXN0YW1wIjoxNDc3NjQ5NzU3LjYyMzM0MTh9MUIxQkM4QUJEMzJGREU2N0VBN0IyQkMzRkQ0MkM4QkM2RTE2M0FEQUE4N0NCNzZCNTUwMzRCRjUxRjQ4QTNFQg==";
            var clientSecret = "test_secret";

            BigCommerceDataProvider.ReadBigCommerceData(signedPayload, clientSecret);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SignedPayloadReader_ReadStore_InvalidHMACSignature()
        {
            var signedPayload = "eyJ1c2VyIjp7ImlkIjoxLCJlbWFpbCI6InRlc3RAdGVzdC5jb20ifSwib3duZXIiOnsiaWQiOjEsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSJ9LCJjb250ZXh0Ijoic3RvcmVzIC8gdGVzdCIsInN0b3JlX2hhc2giOiJ0ZXN0IiwidGltZXN0YW1wIjoxNDc3NjQ5NzU3LjYyMzM0MTh9.MAUIxQkM4QUJEMzJGREU2N0VBN0IyQkMzRkQ0MkM4QkM2RTE2M0FEQUE4N0NCNzZCNTUwMzRCRjUxRjQ4QTNFg==";
            var clientSecret = "test_secret";

            BigCommerceDataProvider.ReadBigCommerceData(signedPayload, clientSecret);
        }
    }
}
