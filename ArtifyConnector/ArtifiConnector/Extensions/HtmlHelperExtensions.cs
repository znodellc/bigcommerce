﻿using System;
using System.IO;
using System.Web.Mvc;

namespace ArtifiConnector.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static string ActiveIf(this HtmlHelper html, string action)
        {
            var viewName = Path.GetFileNameWithoutExtension(((RazorView)html.ViewContext.View).ViewPath);
            return viewName.Equals(action, StringComparison.OrdinalIgnoreCase) ? "active" : "";
        }
    }
}