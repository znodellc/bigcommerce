﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients;
using ArtifiConnector.WebClients.Models;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using ArtifiConnector.Api.Models;

namespace ArtifiConnector.Api
{
    public class OrderController : ApiController
    {
        #region Injected Properties

        [Inject]
        public IArtifiWebClient ArtifiWebClient { get; set; }

        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public ICartRepository CartRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        [Inject]
        public IProductRepository ProductRepository { get; set; }

        #endregion Injected Properties

        private IEnumerable<CartItem> GetCartItems(IEnumerable<Product> products, int merchantId, int customerId)
        {
            var cartItems = new List<CartItem>();
            foreach (var product in products)
            {
                var cartItem = CartRepository.Get(product.Id, merchantId, customerId);

                if (cartItem != null)
                {
                    cartItems.Add(cartItem);
                }
            }

            return cartItems;
        }

        [HttpPost]
        [Route("api/order")]
        public async Task<IHttpActionResult> Create()
        {
            try
            {
                var content = await Request.Content.ReadAsStringAsync();

                Logger.LogInformation(content);

                var jobject = JObject.Parse(content);
                var orderId = jobject["data"].Value<int>("id");
                var storeHash = jobject.Value<string>("producer").Split('/').Last();
                var merchant = MerchantRepository.GetMerchantByStoreHash(storeHash);
                var products = await BigCommerceWebClient.GetOrderProductsAsync(orderId, merchant.AccessToken, storeHash);

                List<string> paths = new List<string>();

                var pairs = new Dictionary<int, int>();

                foreach (var product in products)
                {
                    var designId = 0;

                    if (int.TryParse(product.ProductOptions.Where(x => x.DisplayName == "Artifi Design Id").Select(x => x.Value)?.FirstOrDefault(), out designId))
                    {
                        if (designId != 0)
                        {
                            product.DesignId = designId;
                            pairs.Add(designId, product.OrderItemId);
                        }
                    }

                    if (product.IsCustomizable)
                    {
                        var staffNote = new StaffNoteProductInfo(product.Sku, product.DesignId, merchant.WebsiteId, merchant.WebApiClientKey).ToString();
                        paths.Add(staffNote);
                    }
                }

                var designIds = pairs.Keys;

                if (designIds.Any())
                {
                    UpdateStatusOrder(designIds, merchant);

                    foreach (var pair in pairs)
                    {
                        CartRepository.UpdateOrderItemId(pair.Key, pair.Value);
                    }

                    await BigCommerceWebClient.UpdateOrderStaffNotes(orderId, string.Join(Environment.NewLine, paths), merchant.StoreHash, merchant.AccessToken);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }

            return Ok();
        }

        private async Task<string[]> CreateFiles(int designId)
        {
            var paths = new List<string>();
            var response = await ArtifiWebClient.GetProductFileAsync(designId);
            var mapPath = HostingEnvironment.MapPath("~/Files");

            if (response.FileType == FileType.Pdf)
            {
                var path = Path.Combine(mapPath, $"{designId}.pdf");
                paths.Add(path);
                CreatePdfFile(path, response.Bytes);
            }

            var rootPath = new Uri(Request.RequestUri, RequestContext.VirtualPathRoot).OriginalString;

            return paths.Select(x =>
            {
                var builder = new UriBuilder(new Uri(rootPath));
                builder.Path += "/" + x.Substring(x.IndexOf("Files")).ToString();

                return builder.ToString();
            }).ToArray();
        }

        private void CreatePdfFile(string path, byte[] bytes)
        {
            using (var fs = File.Create(path))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }

        private async void UpdateStatusOrder(IEnumerable<int> productIds, Merchant merchant)
        {
            var request = new UpdateStatusOrderRequest
            {
                WebApiClientKey = merchant.WebApiClientKey,
                WebsiteId = merchant.WebsiteId,
                OrderStatus = "Placed",
                CustomizedProductIds = productIds.ToArray()
            };

            await ArtifiWebClient.UpdateStatusOrder(request);
        }
    }
}