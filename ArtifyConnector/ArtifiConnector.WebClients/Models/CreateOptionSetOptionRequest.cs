﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.WebClients.Models
{
    public class CreateOptionSetOptionRequest
    {
        public int OptionSetId { get; set; }
        public int OptionId { get; set; }
        public string DisplayName { get; set; }
        public string AccessToken { get; set; }
        public string StoreHash { get; set; }
    }
}
