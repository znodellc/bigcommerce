IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spGetCartItem') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spGetCartItem 
GO 

Create PROCEDURE spGetCartItem(
@ProductId int,
@MerchantId int,
@CustomerId int
)
AS
BEGIN
	select ci.DesignID, ci.CartItemID from Product as p
	inner join CartItem as ci
	on p.ProductID  = ci.ProductID 
	where p.ProductID = @ProductId 
	and p.MerchantID = @MerchantId
	and ci.CustomerID = @CustomerId
	and ci.OrderItemId is null
END