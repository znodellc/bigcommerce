﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using ArtifiConnector.Data.Models;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using ArtifiConnector.IntegrationTests.Common;

namespace ArtifiConnector.Data.Dapper.IntegrationTests
{
    [TestClass]
    public class CartRepositoryTests : ArtifiConnectorIntegrationTestBase
    {
        [TestInitialize]
        public void Setup()
        {
            base.Initialize();
            base.Cleanup();
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public void CartRepository_UpdatePreviewUrl()
        {
            var cartItem = new CartItem
            {
                CartItemId = "cart_item_id",
                CustomerId = 3,
                DesignId = 4,
                PreviewUrl = "url",
                ProductId = 5
            };

            InitializeCartItem(cartItem);

            _cartRepository.Update(4, "unit_test_url", 5, 12345);

            var designId = _cartRepository.GetPreviewUrl("cart_item_id");

            Assert.AreEqual("unit_test_url", designId);
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public void CartRepository_UpdateDesignIdAndGetDesignIdByOrderItemId()
        {
            var cartItem = new CartItem
            {
                CartItemId = "cart_item_id",
                CustomerId = 3,
                DesignId = 4,
                PreviewUrl = "url",
                ProductId = 5
            };

            InitializeCartItem(cartItem);

            _cartRepository.UpdateOrderItemId(cartItem.DesignId, 15);

            var designId = _cartRepository.GetDesignIdByOrderItemId(15);

            Assert.AreEqual(4, designId);
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public void CartRepository_GetDesignId()
        {
            var cartItem = new CartItem
            {
                CartItemId = "cart_item_id",
                CustomerId = 3,
                DesignId = 4,
                PreviewUrl = "url",
                ProductId = 5
            };

            InitializeCartItem(cartItem);

            var designId = _cartRepository.GetDesignId("cart_item_id");

            Assert.AreEqual(4, designId);
        }

        private void InitializeCartItem(CartItem cartItem)
        {
            var merchant = new Merchant
            {
                Id = 12345,
                AccessToken = "",
                Hash = "",
                IsConfigured = false,
                StoreHash = "",
                WebApiClientKey = "",
                WebsiteId = ""
            };

            _merchantRepository.Add(merchant);

            var product = new Product
            {
                Id = 5,
                IsSet = false,
                Name = "Name",
                Sku = "SKU",
                Url = "url"
            };

            _productRepository.AddProduct(product, 12345);

            _cartRepository.Add(cartItem, 12345);
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public void CartRepository_AddGetRemoveCartItem()
        {
            var cartItem = new CartItem
            {
                CartItemId = "cart_item_id",
                CustomerId = 3,
                DesignId = 4,
                PreviewUrl = "url",
                ProductId = 5
            };

            InitializeCartItem(cartItem);

            var item = _cartRepository.Get(5, 12345, 3);

            Assert.IsNotNull(item);

            _cartRepository.Remove("cart_item_id");

            item = _cartRepository.Get(1, 12345, 1);

            Assert.IsNull(item);
        }
    }
}
