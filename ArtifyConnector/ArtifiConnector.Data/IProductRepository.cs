﻿using ArtifiConnector.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Data
{
    public interface IProductRepository
    {
        Product GetProduct(int productId, int merchantId);
        void AddProduct(Product product, int merchantId);
        void DeleteProduct(int productId, int merchantId);
        Product GetProductBySku(string sku, int merchantId);

        ProductDetails GetProductDetailsByDesignId(int designId);
        ProductDetails GetProductDetailsByProductId(int productId);
        ProductDetails GetProductDetailsByCartItemId(string cartItemId);
        ProductDetails GetProductDetailsByOrderItemId(int orderItemId);
        ProductDetails GetProductDetailsBySku(string sku);
        int GetProductModifierId(int productId, int merchantId);
    }
}