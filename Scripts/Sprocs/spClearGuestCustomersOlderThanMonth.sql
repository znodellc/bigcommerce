IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spClearGuestCustomersOlderThanMonth') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spClearGuestCustomersOlderThanMonth 
GO 

Create PROCEDURE spClearGuestCustomersOlderThanMonth()
AS
BEGIN
	DELETE FROM Customer
	WHERE DateAdd(Month, 1, CreatedDate) < GetDate()
END