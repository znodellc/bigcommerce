﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtifiConnector.Data.Models;

namespace ArtifiConnector.Data.Dapper
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        private string _connectionString;

        public ConfigurationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ApplicationConfiguration GetApplicationConfiguration(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ApplicationConfiguration>(
                    @"select WebApiClientKey, WebsiteId from Merchant
                    where MerchantID = @MerchantId",
                    new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public IEnumerable<DesignConfiguration> GetDesignConfiguration(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<DesignConfiguration>(
                    @"select c.ConfigurationID [Id], cf.[Name] [key], c.[Value], DisplayName from configuration as c
                    inner join ConfigurationField as cf
                    on cf.ConfigurationFieldID = c.ConfigurationFieldID
                    where MerchantID = @MerchantId",
                    new { MerchantId = merchantId });
            }
        }

        public void SaveApplicationConfiguration(string websiteId, string webApiClientKey, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute(
                    @"update Merchant 
                    set WebsiteId = @WebsiteId,
                    WebApiClientKey = @WebApiClientKey
                    where MerchantID = @MerchantID",
                    new { MerchantId = merchantId, WebApiClientKey = webApiClientKey, WebsiteId = websiteId });
            }
        }

        public void SaveDesignConfiguration(IEnumerable<DesignConfiguration> designConfiguration)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {

                foreach (var configuration in designConfiguration)
                {
                    connection.Execute(
                    @"update Configuration 
                    set Value = @Value
                    where ConfigurationId = @ConfigurationId",
                  new { Value = configuration.Value, ConfigurationId = configuration.Id });
                }
            }
        }
    }
}
