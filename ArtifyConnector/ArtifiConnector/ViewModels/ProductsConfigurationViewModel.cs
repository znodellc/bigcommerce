﻿namespace ArtifiConnector.ViewModels
{
    public class ProductsConfigurationViewModel
    {
        public string ButtonLabel { get; set; }
        public string Keyword { get; set; }
    }
}