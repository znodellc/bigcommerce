﻿using ArtifiConnector.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data
{
    public interface IConfigurationRepository
    {
        ApplicationConfiguration GetApplicationConfiguration(int merchantId);
        void SaveApplicationConfiguration(string websiteId, string webApiClientKey, int merchantId);
        IEnumerable<DesignConfiguration> GetDesignConfiguration(int merchantId);

        void SaveDesignConfiguration(IEnumerable<DesignConfiguration> designConfiguration);
    }
}
