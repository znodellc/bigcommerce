﻿namespace ArtifiConnector.WebClients.Models
{
    public class AddModifierRequest
    {
        public string AccessToken { get; set; }
        public int ProductId { get; set; }
        public string StoreHash { get; set; }
    }
}