﻿using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ArtifiConnector.WebClients
{
    public interface IBigCommerceWebClient
    {
        Task<int> AddModifierAsync(AddModifierRequest request);

        Task<int> CreateOption(Option option, string storeHash, string accessToken);

        Task<int> CreateOptionSet(string name, string storeHash, string accessToken);

        Task<int> CreateOptionSetOption(CreateOptionSetOptionRequest request);

        Task<int?> GetCustomerIdByEmail(string email, string storeHash, string accessToken);

        Task<IEnumerable<int>> GetOptionsForOptionSet(int optionSetId, string storeHash, string accessToken);

        Task<Order> GetOrderAsync(int orderId, string accessToken, string storeHash);

        Task<IEnumerable<OrderProduct>> GetOrderProductsAsync(int orderId, string accessToken, string storeHash);

        Task<int?> GetProductOptionSetId(int productId, string storeHash, string accessToken);

        Task<IEnumerable<Product>> GetProductsAsync(int page, string keyword, string accessToken, string storeHash);

        Task<int> GetProductsCountAsync(string keyword, string storeHash, string accessToken);

        Task<JObject> GetProductSkusAsync(int productId, string storeHash, string accessToken);

        Task<ReceiveTokenResponse> ReceiveTokenAsync(ReceiveTokenRequest request);

        Task RemoveModifierAsync(RemoveModifierRequest request);

        Task RemoveOptionSetFromProduct(int optionSetId, int productId, string storeHash, string accessToken);

        Task UpdateOrderStaffNotes(int orderId, string staffNotes, string storeHash, string accessToken);

        Task UpdateProductOptionSet(int optionSetId, int productId, string storeHash, string accessToken);

        Task<bool> OptionExists(int optionId, string storeHash, string accessToken);

        Task<bool> OptionSetExists(int optionSetId, string storeHash, string accessToken);
    }
}