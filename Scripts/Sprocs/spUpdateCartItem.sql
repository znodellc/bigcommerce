IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spUpdateCartItem') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spUpdateCartItem 
GO 

Create PROCEDURE spUpdateCartItem(
	@DesignId int, 
	@PreviewUrl nvarchar(max), 
	@CustomerId int,
	@MerchantId int
)
AS
BEGIN
	UPDATE CartItem SET customerId = @CustomerId, previewUrl = @PreviewUrl WHERE DesignId = @DesignId
END