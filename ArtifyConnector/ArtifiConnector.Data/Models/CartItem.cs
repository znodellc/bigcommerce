﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Data.Models
{
    public class CartItem
    {
        public int DesignId { get; set; }
        public string CartItemId { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public string PreviewUrl { get; set; }
    }
}