﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.Api;
using ArtifiConnector.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Net.Http.Headers;

namespace ArtifiConnector.Tests.Api
{
    [TestClass]
    public class CartControllerTests
    {
        private CartController _controller;
        private Mock<ICartRepository> _mockCartRepository;
        private Mock<HttpRequestMessage> _request;
        private Mock<IMerchantRepository> _mockMerchantRepository;

        [TestInitialize]
        public void Initialize()
        {
            _mockCartRepository = new Mock<ICartRepository>();
            _mockMerchantRepository = new Mock<IMerchantRepository>();
            _request = new Mock<HttpRequestMessage>();

            _controller = new CartController();
            _controller.ControllerContext = new HttpControllerContext();
            _controller.Request = _request.Object;
            _controller.CartRepository = _mockCartRepository.Object;
            _controller.MerchantRepository = _mockMerchantRepository.Object;
        }

        [TestMethod]
        public void CartController_GetDesignId()
        {
            _mockCartRepository.Setup(x => x.GetDesignId("cart_item_id")).Returns(12);

            dynamic result = _controller.GetDesignId("cart_item_id");
            dynamic content = result.Content;

            Assert.AreEqual(12, content.designId);
        }

        [TestMethod]
        public void CartController_GetDesignId_NotFound()
        {
            var result = _controller.GetDesignId("cart_item_id");

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CartController_GetPreviewUrl_ByCartItem()
        {
            _mockCartRepository.Setup(x => x.GetPreviewUrl("cart_item_id")).Returns("url");
            dynamic result = _controller.GetPreviewUrl("cart_item_id");
            dynamic content = result.Content;

            Assert.AreEqual("url", content.previewUrl);
        }


        [TestMethod]
        public void CartController_GetPreviewUrl_ByCartItem_NotFound()
        {
            var result = _controller.GetPreviewUrl("cart_item_id");

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CartController_GetPreviewUrl_ByCartItemAndProductId()
        {
            _mockCartRepository.Setup(x => x.GetPreviewUrl(1, 1)).Returns("url");

            dynamic result = _controller.GetPreviewUrl(1, 1);
            dynamic content = result.Content;

            Assert.AreEqual("url", content.previewUrl);
        }


        [TestMethod]
        public void CartController_GetPreviewUrl_ByCartItemAndProductId_NotFound()
        {
            var result = _controller.GetPreviewUrl(1, 1);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CartController_GetPreviewUrlByOrderItemId()
        {
            _mockCartRepository.Setup(x => x.GetPreviewUrlByOrderItemId(1)).Returns("url");

            dynamic result = _controller.GetPreviewUrlByOrderItemId(1);
            dynamic content = result.Content;

            Assert.AreEqual("url", content.previewUrl);
        }


        [TestMethod]
        public void CartController_GetPreviewUrlByOrderItemId_NotFound()
        {
            var result = _controller.GetPreviewUrlByOrderItemId(1);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CartController_Add_OK()
        {
            _request.Object.Content = new StringContent(Resources.CartController_Add_Content);
            _mockCartRepository.Setup(x => x.Add(It.IsNotNull<CartItem>(), 12345));
            _mockMerchantRepository.Setup(x => x.GetMerchantIdByHash("test")).Returns(12345);
            _request.Object.Headers.Authorization = new AuthenticationHeaderValue("test");

            var result = _controller.AddCart().Result as OkResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CartController_Add_BadRequest()
        {
            _request.Object.Headers.Authorization = new AuthenticationHeaderValue("test");
            _request.Object.Content = new StringContent("");
            _mockCartRepository.Setup(x => x.Add(It.IsNotNull<CartItem>(), 12345));
            _mockMerchantRepository.Setup(x => x.GetMerchantIdByHash("test")).Returns(12345);

            var result = _controller.AddCart().Result as BadRequestResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CartController_GetDesignIdByOrderItemId_Ok()
        {
            _mockCartRepository.Setup(x => x.GetDesignIdByOrderItemId(1)).Returns(12);

            dynamic result = _controller.GetDesignIdByOrderItemId(1);
            dynamic content = result.Content;

            Assert.AreEqual(12, content.designId);
        }
    }
}
