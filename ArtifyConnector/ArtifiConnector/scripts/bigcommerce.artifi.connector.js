var BigCommerceArtifi = BigCommerceArtifi || {};

BigCommerceArtifi.Constants = {
    cartItemsCookieName: 'bigcommerce.artifi.connector.cartItems',
    customerCookieName: 'bigcommerce.artifi.connector.customer'
};

BigCommerceArtifi.Application = (function ($) {
    'use strict';

    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        merchantId,
        websiteId,
        webApiClientKey,
        applicationHost;

    function init(config) {
        merchantId = config.merchantId;
        websiteId = decode(config.websiteId);
        webApiClientKey = decode(config.webApiClientKey);
        applicationHost = config.applicationHost;

        $(window)
            .ready(loadCookieLibrary());
    }

    function loadCookieLibrary() {
        var script_cycle = document.createElement('script');

        script_cycle.setAttribute("type", "text/javascript");
        script_cycle.setAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js");
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_cycle);
    }

    function getWebsiteId() {
        return websiteId;
    }

    function getWebApiClientKey() {
        return webApiClientKey;
    }

    function sendRequest(endpoint, method, payload, callback, callbackData) {
        $.ajax({
            type: method,
            url: applicationHost + endpoint,
            dataType: 'json',
            async: true,
            data: payload,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', merchantId);
            },
            success: function (response) {
                callback(response, callbackData);
            }
        });
    }

    function decode(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = keyStr.indexOf(input.charAt(i++));
            enc2 = keyStr.indexOf(input.charAt(i++));
            enc3 = keyStr.indexOf(input.charAt(i++));
            enc4 = keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }

        return _utf8_decode(output);
    }

    function _utf8_decode(utftext) {
        var string = "";
        var i = 0;
        var c = 0, c2 = 0, c3 = 0;

        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }

        return string;
    }

    function getBaseIntergrationValues() {
        var integrationValues = {};

        integrationValues.websiteId = getWebsiteId();
        integrationValues.webApiclientKey = getWebApiClientKey();
        integrationValues.divId = 'bigcommerce-artifi-iframe-container';

        return integrationValues;
    }

    function getUrlParameter(searchedParameterName) {
        var pageUrl = decodeURIComponent(window.location.search.substring(1)),
            variables = pageUrl.split('&'),
            parameterName,
            i;

        for (i = 0; i < variables.length; i++) {
            parameterName = variables[i].split('=');

            if (parameterName[0] === searchedParameterName) {
                return parameterName[1] === undefined ? true : parameterName[1];
            }
        }

        return false;
    }

    function generateUniqueIdentifier() {
        return guid4() + guid4() + '-' + guid4() + '-' + guid4() + '-' +
            guid4() + '-' + guid4() + guid4() + guid4();
    }

    function guid4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return {
        init: function (config) {
            return init(config);
        },
        sendRequest: function (endpoint, method, payload, callback, callbackData) {
            return sendRequest(endpoint, method, payload, callback, callbackData);
        },
        getWebsiteId: function () {
            return getWebsiteId();
        },
        getWebApiClientKey: function () {
            return getWebApiClientKey();
        },
        getBaseIntergrationValues: function() {
            return getBaseIntergrationValues();
        },
        getUrlParameter: function(searchedParameterName) {
            return getUrlParameter(searchedParameterName);
        },
        generateUniqueIdentifier: function() {
            return generateUniqueIdentifier();
        }
    };
})(window.jQuery);

BigCommerceArtifi.Customer = (function ($) {
    'use strict';

    var customerEmail,
        customerId,
        bigcommerceCustomerId,
        sessionId;

    function init(email) {
        $(window)
            .ready(handleCustomerLogout());

        var interval = setInterval(function() {
            if (typeof Cookies !== 'undefined') {
                clearInterval(interval);

                $(window)
                    .ready(fetchCustomer(email, handleCustomerInitResponse));
            }
        }, 100);
    }

    function handleCustomerInitResponse(email, response) {
        customerId = response.customerId;
        bigcommerceCustomerId = response.bigCommerceCustomerId;

        var customer = {};
        customer.email = email;
        customer.customerId = customerId;
        customer.bigCommerceCustomerId = bigcommerceCustomerId;

        Cookies.set(BigCommerceArtifi.Constants.customerCookieName, JSON.stringify(customer));
    }

    function fetchCustomer(email, callback) {
        var interval = setInterval(function() {
            if (typeof Cookies !== 'undefined') {
                clearInterval(interval);

                var customer = Cookies.get(BigCommerceArtifi.Constants.customerCookieName);
                if (customer) {
                    customer = JSON.parse(customer);
                }

                if (customer && (!email || email === customer.email)) {
                    return callback(customer.email, customer);
                }

                if (!email) {
                    email = BigCommerceArtifi.Application.generateUniqueIdentifier();
                }

                var endpoint = 'customer?email=' + email,
                    method = 'GET',
                    payload = '';

                if (customer) {
                    endpoint = 'customer?customerId=' + customer.customerId;
                    method = 'PUT';
                    payload = JSON.stringify({'email': email});
                }

                BigCommerceArtifi.Application.sendRequest(
                    endpoint,
                    method,
                    payload,
                    function (response) {
                        callback(email, response);
                    }
                );
            }
        }, 100);
    }

    function handleCustomerLogout() {
        /*var attempt = 1,
            interval = setInterval(function() {
            var linksCollection = $('a'),
                pattern = /\/login.php\?action=logout/g;
            linksCollection.each(function (index, element) {
                var linkObject = $(element),
                    hrefValue = linkObject.attr('href');

                if (!hrefValue) {
                    // Continue to next iteration.
                    return true;
                }

                var result = pattern.exec(hrefValue);
                if (result !== null) {
                    linkObject.on('click', function () {
                        window.console.log('removing cookie');
                        Cookies.remove(BigCommerceArtifi.Constants.customerCookieName);
                    });
                    clearInterval(interval);
                }
            });

            if (attempt >= 3) {
                clearInterval(interval);
            }

            attempt += 1;
        }, 100);*/
    }

    function getCustomerEmail() {
        return customerEmail;
    }

    function getCustomerId() {
        return customerId;
    }

    function getBigcommerceCustomerId() {
        return bigcommerceCustomerId;
    }

    return {
        init: function(email) {
            return init(email);
        },
        fetchCustomer: function (email, callback) {
            return fetchCustomer(email, callback);
        },
        getCustomerEmail: function() {
            return getCustomerEmail();
        },
        getCustomerId: function() {
            return getCustomerId();
        },
        getBigcommerceCustomerId: function() {
            return getBigcommerceCustomerId();
        }
    };
})(window.jQuery);

BigCommerceArtifi.ProductPage = (function ($) {
    'use strict';

    var productId,
        customerEmail,
        productSku,
        productOptions,
        artifiAddToCartEventObject;

    function init(config) {
        productId = config.productId;
        customerEmail = config.customerEmail;

        $(window)
            .ready(checkIfProductIsCustomizable())
            .ready(initRequestsObserver())
            //.ready(BigCommerceArtifi.Customer.init(customerEmail))
            .ready(hideArtifiDesignIdField());
    }

    function initRequestsObserver() {
        var pattern = /\/cart.php/g;

        $(window.document).ajaxSuccess(function(event, xhr, settings) {
            var result = pattern.exec(settings.url);
            if (result !== null) {
                addToCartEventHandler();
            }
        });
    }

    function hideArtifiDesignIdField() {
        var field = getArtifiDesignIdFieldObj();
        $(field)
            .val(0)
            .closest('.productAttributeRow')
            .hide();
    }

    function getArtifiDesignIdFieldObj() {
        var fieldObj = false,
            pattern = /Artifi\sDesign\sId/g;

        $('.productAttributeLabel').each(function() {
            var html = $(this).html(),
                result = pattern.exec(html);
            if (result !== null) {
                var fieldContainer = $(this).siblings('.productAttributeValue');
                if (fieldContainer.length === 1) {
                    fieldObj = fieldContainer.find('input')[0];
                }
            }
        });

        return fieldObj;
    }

    function addToCartEventHandler() {
        var div = $('<div>');
        div.load('/cart.php form[name=cartForm]', function(){
            var cartItemRows = $(this)
                .find('form[name=cartForm]')
                .find('> table')
                .find('> tbody')
                .find('> tr'),
                cartItems = Cookies.get(BigCommerceArtifi.Constants.cartItemsCookieName),
                endpoint = 'cart';

            if (cartItems) {
                cartItems = JSON.parse(cartItems);
            } else {
                cartItems = [];
            }

            cartItemRows.each(function() {
                var row = $(this),
                    cartItemId = row.attr('data-cart-item-id'),
                    productId = row.attr('data-product-id'),
                    productSku = row.attr('data-product-sku');

                if (cartItems.indexOf(cartItemId) > -1) {
                    // Continue to next iteration.
                    return true;
                }

                cartItems.push(cartItemId);

                if (!artifiAddToCartEventObject) {
                    return false;
                }
                if (!artifiAddToCartEventObject.data) {
                    return false;
                }

                var artifiProductSku = artifiAddToCartEventObject.data.sku;
                if (artifiProductSku.toLowerCase() !== productSku.toLowerCase()) {
                    return true;
                }

                BigCommerceArtifi.Application.sendRequest(
                    endpoint,
                    'POST',
                    JSON.stringify({
                        'productId': productId,
                        'cartItemId': cartItemId,
                        'designId': artifiAddToCartEventObject.data.custmizeProductId,
                        'previewUrl': artifiAddToCartEventObject.data.savedDesignStandardImages[0],
                        'customerId': BigCommerceArtifi.Customer.getCustomerId()
                    }),
                    function() {
                        artifiAddToCartEventObject = null;
                    }
                );

                // Break each loop.
                return false;

            });

            Cookies.set(BigCommerceArtifi.Constants.cartItemsCookieName, JSON.stringify(cartItems));
        });
    }

    function checkIfProductIsCustomizable() {
        var endpoint = 'products/' + productId + '/iscustomized';
        BigCommerceArtifi.Application.sendRequest(
            endpoint,
            'GET',
            '',
            prepareToInitArtifiDesigner
        );
    }

    function prepareToInitArtifiDesigner(response) {
        BigCommerceArtifi.Customer.fetchCustomer(customerEmail, function(customer) {
            initArtifiDesigner(response);
        });
    }

    function registerArtifiEventHandler() {
        if (window.addEventListener) {
            addEventListener("message", artifiEventHandler, false);
        } else {
            attachEvent("onmessage", artifiEventHandler);
        }
    }

    function artifiEventHandler(eventObject) {
        if (eventObject === null || typeof eventObject !== 'object') {
            return false;
        }

        var eventData = {};

        try {
            eventData = JSON.parse(eventObject.data);
        } catch (e) {
            return false;
        }

        switch (eventData.action) {
            case 'add-to-cart':
                artifiAddToCartEventHandler(eventData);
                break;
            case 'sku-changed':
                artifiSkuChangedEventHandler(eventData);
                break;
        }
    }

    function artifiAddToCartEventHandler(eventData) {
        artifiAddToCartEventObject = eventData;

        var field = getArtifiDesignIdFieldObj();
        $(field).val(artifiAddToCartEventObject.data.custmizeProductId);

        var designId = isInEditMode();
        if (designId) {
            var endpoint = 'cart';
            BigCommerceArtifi.Application.sendRequest(
                endpoint,
                'PUT',
                JSON.stringify({
                    'designId': artifiAddToCartEventObject.data.custmizeProductId,
                    'previewUrl': artifiAddToCartEventObject.data.savedDesignStandardImages[0],
                    'customerId': BigCommerceArtifi.Customer.getCustomerId()
                }),
                function() {
                    document.location.href = '/cart.php';
                }
            );
        } else {
            toggleArtifiDesigner();
            $('input.add-to-cart[type=submit]').trigger('click');
        }
    }

    function artifiSkuChangedEventHandler(eventData) {
        var newSku = eventData.data.sku;

        if (!productOptions[newSku]) {
            return false;
        }

        $.each(productOptions[newSku], function(index, sku) {
            $('input[name="attribute[' + sku.product_option_id + ']"][value="' + sku.option_value_id + '"]')
                .trigger('click');
        });

        var data = $('#productDetailsAddToCartForm').serializeArray();
        data.push({
            name: 'w',
            value: 'getProductAttributeDetails'
        });
        data = $.param(data);

        $.ajax({
            url: '/remote.php',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
                if (response.success && response.details) {
                    $('#ProductDetails').updateProductDetails(response.details);
                }
            }
        });
    }

    function fetchProductOptions() {
        var endpoint = 'products/' + productId + '/skus';
        BigCommerceArtifi.Application.sendRequest(
            endpoint,
            'GET',
            '',
            function(response) {
                productOptions = response;
            }
        );
    }

    function initArtifiDesigner(response) {
        if (response.Result === 1) {
            productSku = response.ProductSku;

            insertCustomizeButton(response.CustomizedButtonLabel);
            insertArtifiDesigner(response.ProductSku);
            registerArtifiEventHandler();
            fetchProductOptions();
        }
    }

    function toggleArtifiDesigner() {
        $('#bigcommerce-artifi-background').toggle();
        $('#bigcommerce-artifi-iframe-container').toggle();
    }

    function insertCustomizeButton(buttonLabel) {
        var html = '<div style="display:inline-block;margin-left:10px;" id="" class="">';
        html += '<input value="' + buttonLabel + '" type="button" class="btn alt" title="' + buttonLabel + '" onclick="BigCommerceArtifi.ProductPage.toggleArtifiDesigner()">';
        html += '</div>';

        $('#ProductDetails').find('.addto').append(html);
    }

    function isInEditMode() {
        var designId = BigCommerceArtifi.Application.getUrlParameter('designId');

        return parseInt(designId);
    }

    function insertArtifiDesigner(productSku) {
        var src = 'https://integrationdesigner.artifi.net/Designer?',
            customerId = BigCommerceArtifi.Customer.getCustomerId(),
            isGuest = customerId ? 'false' : 'true';

        src += 'productCode=' + productSku;
        src += '&websiteid=' + BigCommerceArtifi.Application.getWebsiteId();
        src += '&userId=' + customerId;
        src += '&isGuest=' + isGuest;
        src += '&webApiclientKey=' + BigCommerceArtifi.Application.getWebApiClientKey();

        window.console.log(src);

        var designId = isInEditMode();
        if (designId > 0) {
            src += '&customizedProductId=' + designId;
        }

        var height = $('#ProductDetails').height(),
            width = $('#ProductDetails').width();

        var iframe = document.createElement('iframe');
        iframe.src = src;
        iframe.height = '663px';
        iframe.width = width + 'px';
        iframe.frameBorder = 0;
        iframe.style.position = 'relative';

        var background = $('<div>');
        background.attr('id', 'bigcommerce-artifi-background');
        background.css({
            'display': 'none',
            'background': 'black',
            'opacity': '0.5',
            'height': $('div.page').height() + 'px',
            'width': '100%',
            'position': 'absolute',
            'z-index': '99',
            'text-align': 'center'
        });

        var popupHeader = $('<div>');
        popupHeader.css({
            'background': 'linear-gradient(to bottom, #727a82 0%, #54586a 100%)',
            'height': '50px',
            'width': '100%'
        });

        var popupIframeOverlay = $('<div>');
        popupIframeOverlay.css({
            'height': '710px',
            'width': '100%',
            'position': 'absolute',
            'text-align': 'center',
            'vertical-align': 'middle',
            'line-height': '710px'
        });
        popupIframeOverlay.html('Loading...');

        var closeButton = $('<a>');
        closeButton.attr('id', 'bigcommerce-artifi-popup-header-close-button');
        closeButton.css({
            'color': 'white',
            'position': 'absolute',
            'right': '20px',
            'top': '16px'
        });
        closeButton.html('[Close]');
        closeButton.attr('href', 'javascript:void(0)');
        closeButton.attr('onclick', 'BigCommerceArtifi.ProductPage.toggleArtifiDesigner()');

        popupHeader.append(closeButton);

        var iframeContainer = $('<div>');
        iframeContainer.attr('id', 'bigcommerce-artifi-iframe-container');
        iframeContainer.css({
            'display': 'none',
            'background': 'white',
            'height': '710px',
            'width': width + 'px',
            'position': 'absolute',
            'z-index': '100',
            'margin': '10% auto 0 auto',
            'top': '0',
            'left': '0',
            'bottom': '0',
            'right': '0'
        });

        $('body')
            .prepend(background)
            .prepend(iframeContainer);
        $('#bigcommerce-artifi-iframe-container')
            .append(popupHeader)
            .append(popupIframeOverlay)
            .append(iframe);

        if (designId > 0) {
            toggleArtifiDesigner();
        }
    }

    return {
        init: function (config) {
            return init(config);
        },
        toggleArtifiDesigner: function () {
            return toggleArtifiDesigner();
        }
    };
})(window.jQuery);

BigCommerceArtifi.CartPage = (function ($) {
    'use strict';

    function init() {
        $(window)
            .ready(fetchCartItemsDetailsId())
            .ready(hideArtifiDesignId())
            .ready(initRequestsObserver());
    }

    function fetchCartItemsDetailsId() {
        var cartItemsCollectionSelector = $('.CartContents')
            .find('> tbody')
            .find('> tr');

        cartItemsCollectionSelector.each(function () {
            var element = $(this),
                productId = element.attr('data-product-id'),
                productSku = element.attr('data-product-sku'),
                cartItemId = element.attr('data-cart-item-id');

            var endpoint = 'product/productDetails?cartItemId=' + cartItemId;
            BigCommerceArtifi.Application.sendRequest(
                endpoint,
                'GET',
                '',
                processFetchedCartItemDetails,
                element
            );

            element
                .find('.CartRemoveLink')
                .attr('onclick', 'BigCommerceArtifi.CartPage.removeCartItem("' + cartItemId + '");return false;');
        });
    }

    function hideArtifiDesignId() {
        var labelsCollectionSelector = $('.CartContents')
            .find('> tbody')
            .find('> tr')
            .find('label'),
            pattern = /Artifi\sDesign\sId/g;

        labelsCollectionSelector.each(function() {
            var html = $(this).html(),
                result = pattern.exec(html);
            if (result !== null) {
                $(this)
                    .closest('tr')
                    .remove();
            }
        });
    }

    function initRequestsObserver() {
        var pattern = /w=editconfigurablefieldsincart/g;

        $(window.document).ajaxSuccess(function(event, xhr, settings) {
            var result = pattern.exec(settings.url);
            if (result !== null) {
                hideArtifiDesignIdPopupField();
            }
        });
    }

    function hideArtifiDesignIdPopupField() {
        var labelsCollectionSelector = $('#CartEditProductFieldsForm')
            .find('label'),
            pattern = /Artifi\sDesign\sId/g;

        labelsCollectionSelector.each(function() {
            var html = $(this).html(),
                result = pattern.exec(html);
            if (result !== null) {
                $(this)
                    .closest('.productAttributeRow')
                    .remove();
            }
        });
    }


    function processFetchedCartItemDetails(response, element) {
        insertButtons(response, element);
        replaceThumbnail(response, element);
    }

    function insertButtons(response, element) {
        element.attr('data-design-id', response.DesignId);

        var html = '<div style="margin-top:10px;">';
        html += '<a style="margin-right:5px;" href="#" title="Edit" onclick="BigCommerceArtifi.CartPage.editCartItem(\'' + response.DesignId + '\');return false;">Edit</a>';
        html += '|';
        html += '<a style="margin-left:5px;" href="#" title="Preview" onclick="BigCommerceArtifi.CartPage.previewCartItem(\'' + response.DesignId + '\'); return false;">Preview</a>';
        html += '</div>';

        element
            .find('.ProductName')
            .append(html);

        element
            .find('a:contains("Change")')
            .remove();
    }

    function replaceThumbnail(response, element) {
        var image = new Image();
        image.src = response.PreviewUrl;
        image.onload = function () {
            element
                .find('.CartThumb')
                .find('img')
                .attr('src', response.PreviewUrl);
        };
    }

    function removeCartItem(cartItemId) {
        if(window.confirm(lang.CartRemoveConfirm)) {
            var endpoint = 'cart',
                cartItems = Cookies.get(BigCommerceArtifi.Constants.cartItemsCookieName);

            if (cartItems) {
                cartItems = JSON.parse(cartItems);
            } else {
                cartItems = [];
            }

            BigCommerceArtifi.Application.sendRequest(
                endpoint,
                'DELETE',
                JSON.stringify({
                    'cartItemId': cartItemId,
                }),
                function (response) {
                    var index = cartItems.indexOf(cartItemId);
                    if (index > -1) {
                        cartItems.splice(index, 1);
                    }

                    document.location.href = 'cart.php?action=remove&item=' + cartItemId;
                }
            );

            return false;
        }
    }

    function previewCartItem(designId, useProductCode) {
        var productSku = $('.CartContents')
            .find('tbody')
            .find('tr[data-design-id=' + designId + ']')
            .attr('data-product-sku');

        var artifiIntegrationValues = BigCommerceArtifi.Application.getBaseIntergrationValues(),
            customerId = BigCommerceArtifi.Customer.getCustomerId(),
            isGuest = customerId ? false : true;

        artifiIntegrationValues.designId = designId;
        artifiIntegrationValues.productCode = productSku;
        artifiIntegrationValues.userId = customerId.toString();
        artifiIntegrationValues.isGuest = isGuest;

        var src = Artifi.getPreviewUrl(artifiIntegrationValues),
            width = 1170,
            height = 540,
            maxWidth = parseInt($(window).innerWidth),
            maxHeight = parseInt($(window).innerHeight);

        if (!useProductCode) {
            src = src.replace('productCode', 'SKU');
        }

        if (width > maxWidth) {
            width = maxWidth;
        }

        if (height > maxHeight) {
            height = maxHeight;
        }

        window.open(src, '', 'width=' + width + 'px,height=' + height + 'px');
    }

    function editCartItem(designId) {
        var productLink = $('.CartContents')
            .find('tbody')
            .find('tr[data-design-id=' + designId + ']')
            .attr('data-product-link');

        document.location.href = productLink + '?designId=' + designId;
    }

    return {
        init: function () {
            init();
        },
        removeCartItem: function (cartItemId) {
            return removeCartItem(cartItemId);
        },
        previewCartItem: function (designId, useProductCode) {
            return previewCartItem(designId, useProductCode);
        },
        editCartItem: function (designId) {
            return editCartItem(designId);
        }
    };
})(window.jQuery);

BigCommerceArtifi.CustomerOrder = (function ($) {
    'use strict';

    function init() {
        $(window)
            .ready(fetchDesignId());
    }

    function fetchDesignId() {
        var orderItemsCollectionSelector = $('.CartContents:first-of-type')
            .find('tbody')
            .find('tr');

        orderItemsCollectionSelector.each(function () {
            var element = $(this),
                orderItemId = element.attr('data-order-item-id');

            var endpoint = 'product/productDetails?orderItemId=' + orderItemId;
            BigCommerceArtifi.Application.sendRequest(
                endpoint,
                'GET',
                '',
                insertButtons,
                element
            );
        });
    }

    function insertButtons(response, element) {
        element.attr('data-design-id', response.DesignId);
        element.attr('data-product-sku', response.Sku);

        var html = '<div style="margin-left:10px;display:inline-block;">(';
        html += '<a style="" href="#" title="Reorder" onclick="BigCommerceArtifi.CustomerOrder.reorderOrderItem(\'' + response.DesignId + '\'); return false;">Reorder</a>, ';
        html += '<a style="" href="#" title="Preview" onclick="BigCommerceArtifi.CustomerOrder.previewOrderItem(\'' + response.DesignId + '\'); return false;">Preview</a>';
        html += ')</div>';

        element
            .find('a')
            .after(html);
    }

    function previewOrderItem(designId) {
        BigCommerceArtifi.CartPage.previewCartItem(designId, 1);
    }

    function reorderOrderItem(designId) {
        var customerId = BigCommerceArtifi.Customer.getCustomerId(),
            endpoint = 'https://integrationdesigner.artifi.net/Designer/Services/GetReorder';
        endpoint += '?designId=' + designId;
        endpoint += '&userId=' + customerId;
        endpoint += '&websiteId=' + BigCommerceArtifi.Application.getWebsiteId();
        endpoint += '&webApiClientKey=' + BigCommerceArtifi.Application.getWebApiClientKey();

        $.ajax({
            type: 'GET',
            url: endpoint,
            dataType: 'json',
            async: true,
            success: function (response) {
                reorderOrderItemCallbackHandler(response);
            }
        });
    }

    function reorderOrderItemCallbackHandler(response) {
    }

    return {
        init: function () {
            init();
        },
        previewOrderItem: function (designId) {
            return previewOrderItem(designId);
        },
        reorderOrderItem: function (designId) {
            return reorderOrderItem(designId);
        }
    };
})(window.jQuery);

BigCommerceArtifi.SavedDesigns = (function ($) {
    'use strict';

    function init() {
        $(window)
            .ready(BigCommerceArtifi.Customer.fetchCustomer(false, initSavedDesigns));
    }

    function initSavedDesigns(email, customer) {
        var artifiIntegrationValues = BigCommerceArtifi.Application.getBaseIntergrationValues();

        artifiIntegrationValues.userId = customer.customerId;
        artifiIntegrationValues.successCallback = savedDesignsSuccessCallback;
        artifiIntegrationValues.errorCallback = savedDesignsErrorCallback;

        Artifi.GetCustomizedProductByUserId(artifiIntegrationValues);
    }

    function savedDesignsSuccessCallback(response) {
        if (response.Response === 'Error') {
            savedDesignsErrorCallback(response);

            return false;
        }

        var container = $('.PageContent');

        $.each(response.Data, function(index, design) {
            var productContainer = $('<div>');

            productContainer.css({
                'float': 'left',
                'margin-right': '10px',
                'margin-bottom': '20px'
            });

            var productImageContainer = $('<div>'),
                productImage = $('<img>').attr(
                    'src',
                    design.CustomDesignModelList[0].ThumbnailImagePath
                );

            productImage.css({'margin-right': '10px'});

            productImageContainer
                .append(productImage)
                .css({
                   'float': 'left'
                });

            var productDescriptionContainer = $('<div>'),
                productCode = $('<p>').text('SKU: ' + design.SKU),
                productDesignId = $('<p>').text('Design Id: ' + design.Id);

            productCode.css({'margin-bottom': '0'});

            productDescriptionContainer
                .append(productCode)
                .append(productDesignId)
                .css({
                    'float': 'left'
                });

            productContainer
                .append(productImageContainer)
                .append(productDescriptionContainer);

            var endpoint = 'product/productDetails?designId=' + design.Id;
            BigCommerceArtifi.Application.sendRequest(
                endpoint,
                'GET',
                '',
                insertButtons,
                productDescriptionContainer
            );

            container.append(productContainer);
        });
    }

    function insertButtons(response, element) {
        window.console.log(response);

        var addToCart = $('<a>'),
            domain = document.location.hostname,
            url;

        url = document.location.protocol + '//';
        url += domain;
        url += '/products.php?productId=' + response.ProductId + '&designId=' + response.DesignId;

        addToCart
            .attr(
                'onclick',
                'document.location.href="' + url + '";return false;'
            )
            .text('Add To Cart');

        element.append(addToCart);
    }

    function savedDesignsErrorCallback(response) {
        var container = $('.PageContent'),
            errorMessage = $('<p>').text(response.Message);

        container.append(errorMessage);
    }

    return {
        init: function () {
            init();
        }
    };
})(window.jQuery);

BigCommerceArtifi.Application.init({
    'merchantId': '<merchantHash>',
    'websiteId': '<base64-of-artifi-website-id>',
    'webApiClientKey': '<base64-of-artifi-web-api-client-key>',
    'applicationHost': 'https://artifi.brzooz.hostingasp.pl/artificonnector/api/'
});