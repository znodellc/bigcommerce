﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtifiConnector.Data.Models;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace ArtifiConnector.Data.Dapper
{
    public class MerchantRepository : IMerchantRepository
    {
        private readonly string _connectionString;

        public MerchantRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Merchant merchant)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("spInsertMerchant", commandType: CommandType.StoredProcedure, param: new { Id = merchant.Id, AccessToken = merchant.AccessToken, Hash = merchant.Hash, StoreHash = merchant.StoreHash });
            }
        }

        public void Configure(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET Configured = 1 WHERE MerchantId = @MerchantId", new { MerchantId = merchantId });
            }
        }

        public Merchant Get(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Merchant>("SELECT MerchantID Id, AccessToken, WebApiClientKey, WebsiteId, StoreHash, Hash, Configured IsConfigured FROM Merchant Where MerchantID = @MerchantId", new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public Merchant GetMerchantByStoreHash(string storeHash)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Merchant>("SELECT MerchantID Id, AccessToken, WebApiClientKey, WebsiteId, StoreHash, Hash, Configured IsConfigured FROM Merchant Where StoreHash = @StoreHash", new { StoreHash = storeHash }).FirstOrDefault();
            }
        }

        public string GetMerchantHashById(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT Hash FROM Merchant Where MerchantId = @MerchantId", new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public int GetMerchantIdByHash(string hash)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<int>("SELECT MerchantID Id FROM Merchant Where Hash = @Hash", new { Hash = hash }).FirstOrDefault();
            }
        }

        public void Remove(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET Configured = 0 WHERE MerchantId = @MerchantId", new { MerchantId = merchantId });
            }
        }

        public void UpdateAccessToken(int merchantId, string accessToken)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET AccessToken = @AccessToken WHERE MerchantId = @MerchantId", new { AccessToken = accessToken, MerchantId = merchantId });
            }
        }

        public Options GetOptions(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Options>("SELECT OptionId, OptionSetId FROM Merchant WHERE MerchantId = @MerchantId", new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public void SetOptionId(int optionId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET OptionId = @OptionId WHERE MerchantId = @MerchantId", new { MerchantId = merchantId, OptionId = optionId });
            }
        }

        public void SetOptionSetId(int optionSetId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET OptionSetId = @OptionSetId WHERE MerchantId = @MerchantId", new { MerchantId = merchantId, OptionSetId = optionSetId });
            }
        }
    }
}
