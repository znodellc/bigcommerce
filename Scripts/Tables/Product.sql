CREATE TABLE [dbo].[Product](
	[ProductID] [int] NOT NULL,
	[MerchantID] [int] NOT NULL,
	[Sku] [nvarchar](50) NOT NULL,
	[Url] [nvarchar](max) NOT NULL
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Merchant] FOREIGN KEY([MerchantID])
REFERENCES [dbo].[Merchant] ([MerchantID])
GO

ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Merchant]
GO

