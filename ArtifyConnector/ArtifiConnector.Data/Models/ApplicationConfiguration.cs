﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class ApplicationConfiguration
    {
        public string WebsiteId { get; set; }
        public string WebApiClientKey { get; set; }
    }
}
