﻿using ArtifiConnector.Data.Models;
using Newtonsoft.Json;

namespace ArtifiConnector.WebClients.Models
{
    public class ReceiveTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        public string Context { get; set; }
        public User User { get; set; }
        public string Scope { get; set; }
    }
}