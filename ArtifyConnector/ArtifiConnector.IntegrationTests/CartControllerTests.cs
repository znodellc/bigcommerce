﻿using ArtifiConnector.Data.Models;
using ArtifiConnector.IntegrationTests.Common;
using ArtifiConnector.IntegrationTests.Common.WebClients;
using ArtifiConnector.IntegrationTests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.IntegrationTests
{
    [TestClass]
    public class CartControllerTests : ArtifiConnectorIntegrationTestBase
    {
        private ArtifiConnectorWebClient _webClient;

        [TestInitialize]
        public void Setup()
        {
            _webClient = new ArtifiConnectorWebClient(Settings.Default.ArtifiConnectorAddress);
            base.Initialize();
            base.Cleanup();
        }

        private void AddCartItem(CartItem cartItem)
        {
            _merchantRepository.Add(new Merchant { Id = 12345, Hash = "test_hash", AccessToken = "", IsConfigured = false, StoreHash = "", WebApiClientKey = "", WebsiteId = "" });
            _productRepository.AddProduct(new Product { Id = 5, IsSet = false, Name = "name", Sku = "sku", Url = "product_url" }, 12345);
            _webClient.AddCart(cartItem, "test_hash").Wait();

        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public async Task CartController_AddGetDesignId()
        {
            var cartItem = new CartItem { CartItemId = "cart_item_id", CustomerId = 2, DesignId = 3, PreviewUrl = "test_url", ProductId = 5 };
            AddCartItem(cartItem);
            var result = await _webClient.GetDesignId("cart_item_id");

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public async Task CartController_AddGetPreviewUrl()
        {
            var cartItem = new CartItem { CartItemId = "cart_item_id", CustomerId = 2, DesignId = 3, PreviewUrl = "test_url", ProductId = 5 };
            AddCartItem(cartItem);
            var result = await _webClient.GetPreviewUrl("cart_item_id");

            Assert.AreEqual("test_url", result);
        }

        [TestMethod]
        [TestCategory("Integration Tests")]
        public async Task CartController_AddGetPreviewUrlByCustomerAndProduct()
        {
            var cartItem = new CartItem { CartItemId = "cart_item_id", CustomerId = 2, DesignId = 3, PreviewUrl = "test_url2", ProductId = 5 };
            AddCartItem(cartItem);
            var result = await _webClient.GetPreviewUrl(2, 5);

            Assert.AreEqual("test_url2", result);
        }
    }
}
