﻿namespace ArtifiConnector
{
    public interface ILogger
    {
        void LogError(string message);

        void LogInformation(string message);

        void LogWarning(string message);
    }
}