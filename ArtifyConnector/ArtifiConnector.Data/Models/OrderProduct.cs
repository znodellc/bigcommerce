﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class OrderProduct
    {
        public int OrderItemId { get; set; }
        public int DesignId { get; set; }
        public string Sku { get; set; }
        public IEnumerable<ProductOption> ProductOptions { get; set; }

        public bool IsCustomizable { get { return DesignId > 0; } }
    }
}
