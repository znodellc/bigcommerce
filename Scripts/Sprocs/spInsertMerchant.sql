IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spInsertMerchant') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spInsertMerchant 
GO 

Create PROCEDURE spInsertMerchant(

@Id nvarchar(50),
@AccessToken nvarchar(50),
@Hash nvarchar(50),
@StoreHash nvarchar(50)
)
AS
BEGIN
	INSERT INTO Merchant (MerchantId, AccessToken, StoreHash, Hash, Configured) VALUES(@Id, @AccessToken, @StoreHash, @Hash, 0)
	
	INSERT INTO Configuration (MerchantID, ConfigurationFieldID)
	SELECT @Id, ConfigurationFieldId from ConfigurationField
END