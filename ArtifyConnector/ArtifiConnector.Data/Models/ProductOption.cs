﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class ProductOption
    {
        public string DisplayName { get; set; }
        public string Value { get; set; }
    }
}
