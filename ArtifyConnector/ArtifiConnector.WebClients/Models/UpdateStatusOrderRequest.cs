﻿namespace ArtifiConnector.WebClients.Models
{
    public class UpdateStatusOrderRequest
    {
        public int[] CustomizedProductIds { get; set; }
        public string OrderStatus { get; set; }
        public string WebApiClientKey { get; set; }
        public string WebsiteId { get; set; }
    }
}