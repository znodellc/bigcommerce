﻿using ArtifiConnector.WebClients.Models;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ArtifiConnector.WebClients
{
    public class ArtifiWebClient : IArtifiWebClient
    {
        private const string BaseAddress = "http://integrationdesigner.artifi.net/";

        public async Task<GetProductFileResponse> GetProductFileAsync(int designId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(BaseAddress + $"Designer/Asset/DownloadOutputFiles?customizedProductId={designId}");

                FileType fileType = FileType.Unknown;
                byte[] file = null;
                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException();

                var mediaType = result.Content.Headers.ContentType.MediaType;

                switch (mediaType)
                {
                    case "application/pdf":
                        fileType = FileType.Pdf;
                        break;

                    default:
                        var content = await result.Content.ReadAsStringAsync();

                        switch (content)
                        {
                            case "\"Your customized product id is not valid.\"":
                                throw new InvalidOperationException("\"Your customized product id is not valid.\"");

                            case "\"Product is not print ready.\"":
                                throw new InvalidOperationException("\"Product is not print ready.\"");

                            case "\"You entered pdf type is not valid.\"":
                                throw new InvalidOperationException("\"You entered pdf type is not valid.\"");

                            case "\"Your PDF is not created , Please try after some time.\"":
                                throw new InvalidOperationException("\"Your PDF is not created , Please try after some time.\"");
                        }
                        break;
                }

                var stream = await result.Content.ReadAsStreamAsync();

                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    file = ms.ToArray();
                }

                return new GetProductFileResponse
                {
                    FileType = fileType,
                    Bytes = file
                };
            }
        }

        public async Task RemoveCartItem(int designId, string websiteId, string WebApiClientKey)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync(BaseAddress + $"/Designer/Services/DeleteCustomizedProduct?customizedProductId={designId}&websiteId={websiteId}&webApiClientKey={WebApiClientKey}");

                var content = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(content);
            }
        }

        public async Task UpdateStatusOrder(UpdateStatusOrderRequest request)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(BaseAddress + $"Designer/Services/UpdateMultipleCustomizedProductOrderStatus?customizedProductIds={string.Join(",", request.CustomizedProductIds)}&websiteId={request.WebsiteId}&orderStatus={request.OrderStatus}&webApiClientKey={request.WebApiClientKey}");

                var content = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(content);

                var jobject = JObject.Parse(content);

                if (jobject.Value<string>("Response").ToLower() != "success")
                {
                    var message = jobject.Value<string>("Message");
                    throw new InvalidOperationException(message);
                }
            }
        }
    }
}