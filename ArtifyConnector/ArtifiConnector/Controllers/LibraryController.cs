﻿using ArtifiConnector.Data;
using ArtifiConnector.Extensions;
using Ninject;
using System;
using System.Web.Mvc;

namespace ArtifiConnector.Controllers
{
    public class LibraryController : Controller
    {
        [Inject]
        public ILogger Logger { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        public JavaScriptResult Script(string hash)
        {
            try
            {
                var merchantId = MerchantRepository.GetMerchantIdByHash(hash);
                var merchant = MerchantRepository.Get(merchantId);
                var content = System.IO.File.ReadAllText(Server.MapPath("~/scripts/bigcommerce.artifi.connector.js"));

                if (string.IsNullOrWhiteSpace(merchant.WebsiteId))
                    throw new InvalidOperationException("WebsiteId cannot be null or empty.");

                if (string.IsNullOrWhiteSpace(merchant.WebApiClientKey))
                    throw new InvalidOperationException("WebApiClientKey cannot be null or empty.");

                content = content.Replace("<merchantHash>", merchant.Hash);
                content = content.Replace("<base64-of-artifi-website-id>", merchant.WebsiteId.ToBase64());
                content = content.Replace("<base64-of-artifi-web-api-client-key>", merchant.WebApiClientKey.ToBase64());

                return JavaScript(content);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                throw;
            }
        }
    }
}