﻿using ArtifiConnector.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.IntegrationTests.Common.WebClients
{
    public class ArtifiConnectorWebClient
    {
        private string _baseAddress;

        public ArtifiConnectorWebClient(string baseAddress)
        {
            _baseAddress = baseAddress;
        }

        #region Configuration

        public async Task SaveApplicationConfiguration(int merchantId, int websiteId, string webApiClientKey)
        {
            var jsonRequest = JsonConvert.SerializeObject(new
            {
                merchantId = merchantId,
                websiteId = websiteId,
                webApiClientKey = webApiClientKey
            });

            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"{_baseAddress}/api/configuration/application", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        [Obsolete("not ready")]
        public async Task SaveDesignConfiguration(int merchantId, int websiteId, string webApiClientKey)
        {
            var jsonRequest = JsonConvert.SerializeObject(new
            {
                merchantId = merchantId,
                websiteId = websiteId,
                webApiClientKey = webApiClientKey
            });

            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"{_baseAddress}/api/configuration/application", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        #endregion

        #region Cart
        public async Task AddCart(CartItem cartItem, string merchantHash)
        {
            var data = new CartItem
            {
                DesignId = 1,
                CartItemId = "cart_item_id",
                ProductId = 1,
                CustomerId = 1,
                PreviewUrl = "preview_url"
            };

            var jsonRequest = JsonConvert.SerializeObject(cartItem);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("authorization", merchantHash);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"{_baseAddress}/api/cart", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        public async Task DeleteCart(string cartItemId, string merchantHash)
        {
            var jsonRequest = JsonConvert.SerializeObject(new { cartItemId = cartItemId });

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("", merchantHash);

                var request = new HttpRequestMessage
                {
                    Content = new StringContent(jsonRequest, Encoding.UTF8, "application/json"),
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri($"{_baseAddress}/api/cart/delete")
                };

                var result = await client.SendAsync(request);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        public async Task<int> GetDesignId(string cartItemId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_baseAddress}/api/cart/{cartItemId}/designId");
                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<int>("designId");
            }
        }

        public async Task<int> GetDesignIdByOrderItemId(int orderItemId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_baseAddress}/api/cart/order/{orderItemId}/designId");
                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<int>("designId");
            }
        }

        public async Task<string> GetPreviewUrl(int customerId, int productId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_baseAddress}/api/cart/customer/{customerId}/product/{productId}/previewUrl");
                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<string>("previewUrl");
            }
        }

        public async Task<string> GetPreviewUrl(string cartItemId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_baseAddress}/api/cart/{cartItemId}/previewUrl");
                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<string>("previewUrl");
            }
        }

        public async Task<string> GetPreviewUrlByOrderItemId(int orderItemId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_baseAddress}/api/cart/order/{orderItemId}/previewUrl");
                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<string>("previewUrl");
            }
        }

        public async Task UpdateCart(int designId, string previewUrl)
        {
            var jsonRequest = JsonConvert.SerializeObject(new { previewUrl = previewUrl, designId = designId });

            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PutAsync($"{_baseAddress}/api/cart", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        #endregion

        #region Order
        public async Task CreateOrder(string merchantHash, int dataId, string producer)
        {
            var request = new { data = new { id = dataId }, producer = producer };

            var jsonRequest = JsonConvert.SerializeObject(request);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("", merchantHash);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"{_baseAddress}/api/order", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }
        #endregion
    }
}