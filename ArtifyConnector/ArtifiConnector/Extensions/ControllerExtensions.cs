﻿using System.Web.Mvc;

namespace ArtifiConnector.Extensions
{
    public static class ControllerExtensions
    {
        public static string GetRequestUrl(this Controller controller)
        {
            var scheme = controller.Request.Url.Scheme;
            var host = controller.Request.Url.Host;
            var path = controller.Request.Url.LocalPath;
            return $"{scheme}://{host}{path}";
        }
    }
}