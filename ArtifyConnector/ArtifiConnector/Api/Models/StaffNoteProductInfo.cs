﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Api.Models
{
    public class StaffNoteProductInfo
    {
        public string Sku { get; }
        public int DesignId { get; }
        public string PDF { get; }
        public string XML { get; }

        public StaffNoteProductInfo(string sku, int designId, string websiteId, string webApiClientKey)
        {
            Sku = sku;
            DesignId = designId;
            PDF = $"https://integrationadmin.artifi.net/Designer/Services/DownloadOutputFiles?customizedProductId={designId}&websiteId={websiteId}&webApiClientKey={webApiClientKey}";
            XML = $"https://integrationadmin.artifi.net/Designer/Services/GetOrderDetailXml?customizedProductId={designId}&websiteId={websiteId}&webApiClientKey={webApiClientKey}";
        }

        public override string ToString()
        {
            return $"- Sku: {Sku}\n - Design Id: {DesignId}\n - PDF: {PDF}\n - XML: {XML}\n\n";
        }
    }
}