﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients;
using ArtifiConnector.WebClients.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ArtifiConnector
{
    public class OptionsManager
    {
        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        public async Task RemoveArtifiDesignIdOption(int productId, Merchant merchant)
        {
            var options = MerchantRepository.GetOptions(merchant.Id);

            if (!options.OptionSetId.HasValue)
                throw new InvalidOperationException("Option Set Id cannot be null.");

            var productOptionSetId = await BigCommerceWebClient.GetProductOptionSetId(productId, merchant.StoreHash, merchant.AccessToken);
            var optionSetId = MerchantRepository.GetOptions(merchant.Id);

            if (productOptionSetId == optionSetId.OptionSetId)
                await BigCommerceWebClient.RemoveOptionSetFromProduct(options.OptionSetId.Value, productId, merchant.StoreHash, merchant.AccessToken);
        }

        private async Task UpdateExistingOptionSet(int productOptionSetId, Merchant merchant, Option option)
        {
            var optionSetOptions = await BigCommerceWebClient.GetOptionsForOptionSet(productOptionSetId, merchant.StoreHash, merchant.AccessToken);

            if (!optionSetOptions.Contains(option.Id))
            {
                var request = new CreateOptionSetOptionRequest
                {
                    AccessToken = merchant.AccessToken,
                    StoreHash = merchant.StoreHash,
                    OptionSetId = productOptionSetId,
                    OptionId = option.Id,
                    DisplayName = option.DisplayName
                };

                await BigCommerceWebClient.CreateOptionSetOption(request);
            }
        }

        public async Task SetArtifiDesignIdOption(int productId, Merchant merchant)
        {
            var options = MerchantRepository.GetOptions(merchant.Id);

            var option = new Option
            {
                Name = "Artifi Design Id",
                DisplayName = "Artifi Design Id"
            };

            await CreateOptionIfNotExists(option, options, merchant);
            await CreateOptionSetIfNotExists("Artifi", option, options, merchant);

            var productOptionSetId = await BigCommerceWebClient.GetProductOptionSetId(productId, merchant.StoreHash, merchant.AccessToken);

            if (productOptionSetId.HasValue)
            {
                await UpdateExistingOptionSet(productOptionSetId.Value, merchant, option);
            }
            else
            {
                await BigCommerceWebClient.UpdateProductOptionSet(options.OptionSetId.Value, productId, merchant.StoreHash, merchant.AccessToken);
            }
        }

        private async Task CreateOptionSetIfNotExists(string optionSetName, Option option, Options options, Merchant merchant)
        {
            bool optionSetExist = false;

            if (options.OptionSetId.HasValue)
            {
                optionSetExist = await BigCommerceWebClient.OptionSetExists(options.OptionSetId.Value, merchant.StoreHash, merchant.AccessToken);
            }

            if (!optionSetExist)
            {
                options.OptionSetId = await CreateOptionSet(optionSetName, option, merchant);
                MerchantRepository.SetOptionSetId(options.OptionSetId.Value, merchant.Id);
            }
        }

        private async Task CreateOptionIfNotExists(Option option, Options options, Merchant merchant)
        {
            bool optionExist = false;
            if (options.OptionId.HasValue)
            {
                optionExist = await BigCommerceWebClient.OptionExists(options.OptionId.Value, merchant.StoreHash, merchant.AccessToken);
            }

            if (!optionExist)
            {
                options.OptionId = await BigCommerceWebClient.CreateOption(option, merchant.StoreHash, merchant.AccessToken);
                MerchantRepository.SetOptionId(options.OptionId.Value, merchant.Id);
            }

            option.Id = options.OptionId.Value;
        }

        private async Task<int> CreateOptionSet(string optionSetName, Option option, Merchant merchant)
        {
            var optionSetId = await BigCommerceWebClient.CreateOptionSet(optionSetName, merchant.StoreHash, merchant.AccessToken);
            var request = new CreateOptionSetOptionRequest
            {
                AccessToken = merchant.AccessToken,
                StoreHash = merchant.StoreHash,
                OptionSetId = optionSetId,
                OptionId = option.Id,
                DisplayName = option.DisplayName
            };

            await BigCommerceWebClient.CreateOptionSetOption(request);

            return optionSetId;
        }
    }
}