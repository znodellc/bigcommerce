var Artifi = Artifi || {};

Artifi.InstallationGuide = (function ($) {
    'use strict';

    var slidesContainerSelector = '#installation-slides',
        slidesContainer,
        slidesLeftControlButtonSelector = '.left.carousel-control',
        slidesRightControlButtonSelector = '.right.carousel-control',
        slidesIndicatorsContinerSelector = '.carousel-indicators',
        progressBarSelector = '.progress-bar',
        progressBarSlidesCount,
        progressBarSlideProgress;

    function init() {
        slidesContainer = $(slidesContainerSelector);
        $(document)
            .ready(initSlidesLeftControlOnClickHook)
            .ready(initSlidesRightControlOnClickHook)
            .ready(initSlidesIndicatorsOnClickHook)
            .ready(initProgressBar);
    }

    function initSlidesLeftControlOnClickHook() {
        var button = slidesContainer.find(slidesLeftControlButtonSelector);
        button.on('click', slidesLeftControlOnClickHandler);
    }

    function slidesLeftControlOnClickHandler(event) {
        var activeSlideNumber = getActiveSlideNumber();
        if (activeSlideNumber === 1) {
            return false;
        }
        updateProgressBar(activeSlideNumber - 1);
    }

    function initSlidesRightControlOnClickHook() {
        var button = slidesContainer.find(slidesRightControlButtonSelector);
        button.on('click', slidesRightControlOnClickHandler);
    }

    function slidesRightControlOnClickHandler() {
        var activeSlideNumber = getActiveSlideNumber();
        if (activeSlideNumber === progressBarSlidesCount) {
            return false;
        }
        updateProgressBar(activeSlideNumber + 1);
    }

    function initSlidesIndicatorsOnClickHook() {
        progressBarSlidesCount = slidesContainer
            .find(slidesIndicatorsContinerSelector)
            .find('li')
            .on('click', function() { return false; });
    }

    function getActiveSlideNumber() {
        var activeSlideIndicator = slidesContainer
            .find(slidesIndicatorsContinerSelector)
            .find('li.active');

        return parseInt(activeSlideIndicator.attr('data-slide-to')) + 1;
    }

    function initProgressBar() {
        progressBarSlidesCount = slidesContainer
            .find(slidesIndicatorsContinerSelector)
            .find('li')
            .length;
        progressBarSlideProgress = (100 / progressBarSlidesCount).toFixed(2);

        updateProgressBar(1);
    }

    function updateProgressBar(slideNumber) {
        var progressBarProgress = progressBarSlideProgress * slideNumber;
        var progressBar = $(progressBarSelector);
        progressBar
            .attr('aria-valuenow', progressBarProgress)
            .css('width', progressBarProgress + '%');
        progressBar
            .find('span')
            .html(progressBarProgress + '% Complete');
    }

    return {
        init: function () {
            init();
        }
    };
})(window.jQuery);