﻿using System;
using System.Text;

namespace ArtifiConnector.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64(this string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            return Convert.ToBase64String(bytes);
        }
    }
}