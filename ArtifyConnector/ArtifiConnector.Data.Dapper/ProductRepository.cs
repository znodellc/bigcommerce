﻿using ArtifiConnector.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Dapper
{
    public class ProductRepository : IProductRepository
    {
        private readonly string _connectionString;

        public ProductRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public Product GetProduct(int productId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Product>("SELECT ProductId, Sku FROM Product Where ProductId = @ProductId and MerchantId = @MerchantId", new { ProductId = productId, MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public void DeleteProduct(int productId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("spDeleteProduct", commandType: CommandType.StoredProcedure, param: new { ProductId = productId, MerchantId = merchantId });
            }
        }

        public Product GetProductBySku(string sku, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Product>("SELECT ProductId Id, Url FROM Product Where Sku = @Sku and MerchantId = @MerchantId", new { Sku = sku, MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public ProductDetails GetProductDetailsByDesignId(int designId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ProductDetails>(@"
                    select ci.DesignID, ci.ProductID, ci.CartItemID, ci.OrderItemId, p.Sku, ci.PreviewUrl from CartItem ci
                    inner join Product p
                    on ci.ProductID = p.ProductID
                    where ci.DesignID = @DesignId", new { DesignId = designId }).FirstOrDefault();
            }
        }

        public ProductDetails GetProductDetailsByProductId(int productId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ProductDetails>(@"
                    select ci.DesignID, ci.ProductID, ci.CartItemID, ci.OrderItemId, p.Sku, ci.PreviewUrl from CartItem ci
                    inner join Product p
                    on ci.ProductID = p.ProductID
                    where ci.ProductId = @ProductId", new { ProductId = productId }).FirstOrDefault();
            }
        }

        public ProductDetails GetProductDetailsByCartItemId(string cartItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ProductDetails>(@"
                    select ci.DesignID, ci.ProductID, ci.CartItemID, ci.OrderItemId, p.Sku, ci.PreviewUrl from CartItem ci
                    inner join Product p
                    on ci.ProductID = p.ProductID
                    where ci.CartItemId = @CartItemId", new { CartItemId = cartItemId }).FirstOrDefault();
            }
        }

        public ProductDetails GetProductDetailsByOrderItemId(int orderItemId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ProductDetails>(@"
                    select ci.DesignID, ci.ProductID, ci.CartItemID, ci.OrderItemId, p.Sku, ci.PreviewUrl from CartItem ci
                    inner join Product p
                    on ci.ProductID = p.ProductID
                    where ci.OrderItemId = @OrderItemId", new { OrderItemId = orderItemId }).FirstOrDefault();
            }
        }

        public ProductDetails GetProductDetailsBySku(string sku)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<ProductDetails>(@"
                    select ci.DesignID, ci.ProductID, ci.CartItemID, ci.OrderItemId, p.Sku, ci.PreviewUrl from CartItem ci
                    inner join Product p
                    on ci.ProductID = p.ProductID
                    where p.Sku = @Sku", new { Sku = sku }).FirstOrDefault();
            }
        }

        public void AddProduct(Product product, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("INSERT INTO Product VALUES(@ProductId, @MerchantId, @Sku, @Url, @ModifierId)", new { ProductId = product.Id, Sku = product.Sku, MerchantId = merchantId, Url = product.Url, ModifierId = product.ModifierId });
            }
        }

        public int GetProductModifierId(int productId, int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<int>("SELECT ModifierId FROM Product Where ProductId = @ProductId and MerchantId = @MerchantId", new { ProductId = productId, MerchantId = merchantId }).FirstOrDefault();
            }
        }
    }
}
