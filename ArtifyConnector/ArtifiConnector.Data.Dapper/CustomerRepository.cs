﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtifiConnector.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace ArtifiConnector.Data.Dapper
{
    public class CustomerRepository : ICustomerRepository
    {
        private string _connectionString;

        public CustomerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Add(Customer customer)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.ExecuteScalar<int>("INSERT INTO Customer (MerchantId, BigCommerceCustomerId, IsGuest, Email, CreatedDate) VALUES (@MerchantId, @BigCommerceCustomerId, 0, @Email, @CreatedDate) SELECT SCOPE_IDENTITY()", new { MerchantId = customer.MerchantId, BigCommerceCustomerId = customer.BigCommerceCustomerId, Email = customer.Email, CreatedDate = DateTime.Now });
            }
        }

        public int AddGuest(int merchantId, string email)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.ExecuteScalar<int>("INSERT INTO Customer (MerchantId, BigCommerceCustomerId, IsGuest, Email, CreatedDate) VALUES (@MerchantId, NULL, 1, @Email, @CreatedDate) SELECT SCOPE_IDENTITY()", new { MerchantId = merchantId, Email = email, CreatedDate = DateTime.Now });
            }
        }

        public Customer GetCustomerByEmail(string email)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Customer>("SELECT CustomerId Id, MerchantId, BigCommerceCustomerId, IsGuest, Email, CreatedDate FROM Customer WHERE Email=@Email", new { Email = email }).FirstOrDefault();
            }
        }

        public bool CustomerEmailExists(string email)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var result = connection.ExecuteScalar<string>("SELECT Email FROM Customer WHERE Email=@Email", new { Email = email });

                return !string.IsNullOrWhiteSpace(result);

            }
        }

        public void UpdateCustomer(Customer customer)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Customer SET BigCommerceCustomerId = @BigCommerceCustomerId, IsGuest = 0, Email = @Email WHERE CustomerId = @CustomerId", new { BigCommerceCustomerId = customer.BigCommerceCustomerId, CustomerId = customer.Id, Email = customer.Email });
            }
        }
    }
}
