IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spDeleteMerchant') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spDeleteMerchant 
GO 

Create PROCEDURE spDeleteMerchant(

@MerchantId nvarchar(50)
)
AS
BEGIN
	UPDATE Merchant Set Configured = 0 WHERE MerchantId = @MerchantId
END