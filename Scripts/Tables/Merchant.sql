CREATE TABLE [dbo].[Merchant](
	[MerchantID] [int] NOT NULL,
	[AccessToken] [nvarchar](50) NOT NULL,
	[WebApiClientKey] [nvarchar](50),
	[WebsiteId] [nvarchar](50),
	[StoreHash] [nvarchar](50) NOT NULL,
	[Hash] [nvarchar](50) NOT NULL,
	[Configured] [bit] NOT NULL
 CONSTRAINT [PK_Merchant] PRIMARY KEY CLUSTERED 
(
	[MerchantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

