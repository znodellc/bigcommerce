﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Data.Models
{
    public class BigCommerceData
    {
        public User User { get; set; }
        public Owner Owner { get; set; }
        public string Context { get; set; }
        public string StoreHash { get; set; }
        public string Timestamp { get; set; }
    }
}