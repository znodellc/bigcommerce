﻿namespace ArtifiConnector.Models
{
    public class AuthRequest
    {
        public string Code { get; set; }
        public string Context { get; set; }
        public string Scope { get; set; }
    }
}