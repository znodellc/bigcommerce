IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spDeleteProduct') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spDeleteProduct 
GO 

Create PROCEDURE spDeleteProduct(

@ProductId int,
@MerchantId int
)
AS
BEGIN
	DELETE FROM CartItem WHERE ProductId = @ProductId
	DELETE FROM Product WHERE ProductId = @ProductId and MerchantId = @MerchantId
END