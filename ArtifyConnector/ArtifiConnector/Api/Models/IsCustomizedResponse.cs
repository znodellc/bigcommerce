﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Api.Models
{
    public class IsCustomizedResponse
    {
        public int Result { get; set; }
        public string ProductSku { get; set; }
        public string CustomizedButtonLabel { get; set; }
    }
}