﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.WebClients.Models
{
    public class GetProductFileResponse
    {
        public FileType FileType { get; set; }
        public byte[] Bytes { get; set; }
    }
}
