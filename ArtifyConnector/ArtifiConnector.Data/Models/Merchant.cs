﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Data.Models
{
    public class Merchant
    {
        public int Id { get; set; }
        public string AccessToken { get; set; }
        public string WebApiClientKey { get; set; }
        public bool IsConfigured { get; set; }
        public string WebsiteId { get; set; }
        public string StoreHash { get; set; }
        public string Hash { get; set; }
    }
}