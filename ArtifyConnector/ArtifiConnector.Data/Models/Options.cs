﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class Options
    {
        public int? OptionId { get;set;}
        public int? OptionSetId { get; set; }
    }
}
