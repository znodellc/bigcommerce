﻿using ArtifiConnector.Data.Models;

namespace ArtifiConnector.Data
{
    public interface IMerchantRepository
    {
        void Add(Merchant merchant);
        Merchant Get(int merchantId);
        void Configure(int merchantId);

        void Remove(int merchantId);

        int GetMerchantIdByHash(string hash);

        string GetMerchantHashById(int merchantId);

        Merchant GetMerchantByStoreHash(string storeHash);

        void UpdateAccessToken(int merchantId, string accessToken);
        Options GetOptions(int merchantId);
        void SetOptionId(int optionId, int merchantId);
        void SetOptionSetId(int optionSetId, int merchantId);
    }
}