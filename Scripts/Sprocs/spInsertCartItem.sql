IF EXISTS ( SELECT  * FROM sys.objects WHERE   object_id = OBJECT_ID(N'spInsertCartItem') AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE spInsertCartItem 
GO 

Create PROCEDURE spInsertCartItem(
	@ProductId int, 
	@CartItemId nvarchar(50), 
	@DesignId int, 
	@PreviewUrl nvarchar(max), 
	@CustomerId int,
	@MerchantId int
)
AS
BEGIN
	INSERT INTO CartItem (productId, cartItemId, designId, previewUrl, customerId) VALUES(@ProductId, @CartItemId, @DesignId, @PreviewUrl, @CustomerId)
END