﻿using ArtifiConnector.Data.Dapper;
using ArtifiConnector.Data.Models;
using ArtifiConnector.IntegrationTests.Common.WebClients;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.IntegrationTests.Common
{
    public class ArtifiConnectorIntegrationTestBase
    {
        public CartRepository _cartRepository;
        public ProductRepository _productRepository;
        public MerchantRepository _merchantRepository;
        private string _connectionString;

        public void Initialize()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["IntegrationTestsConnectionString"].ConnectionString;

            _cartRepository = new CartRepository(_connectionString);
            _productRepository = new ProductRepository(_connectionString);
            _merchantRepository = new MerchantRepository(_connectionString);
        }

        public void Cleanup()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("DELETE FROM Configuration");
                connection.Execute("DELETE FROM CartItem");
                connection.Execute("DELETE FROM Customer");
                connection.Execute("DELETE FROM Product");
                connection.Execute("DELETE FROM Merchant");
            }
        }
    }
}
