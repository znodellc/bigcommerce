﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public int MerchantId { get; set; }
        public int? BigCommerceCustomerId { get; set; }
        public bool IsGuest { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
