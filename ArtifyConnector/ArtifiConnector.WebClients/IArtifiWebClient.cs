﻿using ArtifiConnector.WebClients.Models;
using System.Threading.Tasks;

namespace ArtifiConnector.WebClients
{
    public interface IArtifiWebClient
    {
        Task RemoveCartItem(int designId, string websiteId, string WebApiClientKey);

        Task UpdateStatusOrder(UpdateStatusOrderRequest request);
        Task<GetProductFileResponse> GetProductFileAsync(int designId);
    }
}