﻿using ArtifiConnector.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiConnector.Data
{
    public interface ICartRepository
    {
        void Add(CartItem item, int merchantId);
        int? GetDesignId(string cartItemId);
        string GetPreviewUrl(string cartItemId);
        string GetPreviewUrlByOrderItemId(int orderItemId);
        string GetPreviewUrl(int customerId, int productId);
        void Remove(string cartItemId);
        void Update(int designId, string previewUrl, int? customerId, int merchantId);
        CartItem Get(int productId, int merchantId, int customerId);
        void UpdateOrderItemId(int designId, int orderItemId);
        int? GetDesignIdByOrderItemId(int orderItemId);
    }
}