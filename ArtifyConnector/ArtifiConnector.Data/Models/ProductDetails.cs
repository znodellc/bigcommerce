﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class ProductDetails
    {
        public int DesignId { get; set; }
        public int ProductId { get; set; }
        public string CartItemId { get; set; }
        public int OrderItemId { get; set; }
        public string Sku { get; set; }
        public string PreviewUrl { get; set; }
    }
}
