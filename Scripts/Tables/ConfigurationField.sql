CREATE TABLE [dbo].[ConfigurationField](
	[ConfigurationFieldID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_ConfigurationField] PRIMARY KEY CLUSTERED 
(
	[ConfigurationFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

