﻿using ArtifiConnector.Api.Models;
using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients;
using ArtifiConnector.WebClients.Models;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace ArtifiConnector.Api
{
    public class ProductsController : ApiController
    {
        #region Injected Properties

        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public IConfigurationRepository ConfigurationRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        [Inject]
        public IProductRepository ProductRepository { get; set; }

        [Inject]
        public OptionsManager OptionsManager { get; set; }
        #endregion Injected Properties

        [Route("api/products/")]
        [HttpPost]
        public async Task<IHttpActionResult> AddProduct()
        {
            try
            {
                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);
                var productId = jobject.Value<int>("id");
                var sku = jobject.Value<string>("sku");
                var merchantId = jobject.Value<int>("merchantId");
                var merchant = MerchantRepository.Get(merchantId);

                var request = new AddModifierRequest
                {
                    ProductId = productId,
                    AccessToken = merchant.AccessToken,
                    StoreHash = merchant.StoreHash
                };

                await OptionsManager.SetArtifiDesignIdOption(productId, merchant);

                var product = new Product
                {
                    Id = productId,
                    Sku = sku,
                    Url = ""
                };

                ProductRepository.AddProduct(product, merchantId);
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [Route("api/products/")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProduct()
        {
            try
            {
                var json = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);
                var productId = jobject.Value<int>("id");
                var merchantId = jobject.Value<int>("merchantId");
                var merchant = MerchantRepository.Get(merchantId);

                var request = new RemoveModifierRequest
                {
                    ProductId = productId,
                    AccessToken = merchant.AccessToken,
                    StoreHash = merchant.StoreHash,
                };

                await OptionsManager.RemoveArtifiDesignIdOption(productId, merchant);

                ProductRepository.DeleteProduct(productId, merchantId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [Route("api/products/sku/{sku}")]
        public IHttpActionResult GetProductBySku(string sku)
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var product = ProductRepository.GetProductBySku(sku, merchantId);
                return Ok(new { productId = product.Id, url = product.Url });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/product/productDetails")]
        public IHttpActionResult GetProductDetails(int? designId = null, int? productId = null, string cartItemId = null, int? orderItemId = null, string sku = "")
        {
            try
            {
                if (designId.HasValue)
                    return HandleResult(ProductRepository.GetProductDetailsByDesignId(designId.Value));

                if (productId.HasValue)
                    return HandleResult(ProductRepository.GetProductDetailsByProductId(productId.Value));

                if (cartItemId != null)
                    return HandleResult(ProductRepository.GetProductDetailsByCartItemId(cartItemId));

                if (orderItemId.HasValue)
                    return HandleResult(ProductRepository.GetProductDetailsByOrderItemId(orderItemId.Value));

                if (!string.IsNullOrWhiteSpace(sku))
                    return HandleResult(ProductRepository.GetProductDetailsBySku(sku));

                return BadRequest();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [Route("api/products")]
        public async Task<IHttpActionResult> GetProducts(int page = 1, string keyword = "")
        {
            try
            {
                var merchantId = int.Parse(Request.Headers.Authorization.ToString());
                var merchant = MerchantRepository.Get(merchantId);
                var products = await BigCommerceWebClient.GetProductsAsync(page, keyword, merchant.AccessToken, merchant.StoreHash);

                if (products == null)
                    return StatusCode(HttpStatusCode.NoContent);

                var count = await BigCommerceWebClient.GetProductsCountAsync(keyword, merchant.StoreHash, merchant.AccessToken);
                foreach (var product in products)
                {
                    product.IsSet = ProductRepository.GetProduct(product.Id, merchantId) != null;
                }

                return Ok(new { data = products, recordsTotal = count, recordsFiltered = count });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [HttpGet]
        [Route("api/products/{productId}/skus")]
        public async Task<IHttpActionResult> GetSkus(int productId)
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var merchant = MerchantRepository.Get(merchantId);
                var result = await BigCommerceWebClient.GetProductSkusAsync(productId, merchant.StoreHash, merchant.AccessToken);

                if (result == null)
                    return StatusCode(HttpStatusCode.NoContent);

                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [Route("api/products/{productId}/isCustomized")]
        [HttpGet]
        public IHttpActionResult IsCustomized(int productId)
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var product = ProductRepository.GetProduct(productId, merchantId);
                var designConfiguration = ConfigurationRepository.GetDesignConfiguration(merchantId);

                if (product == null)
                    return Ok(new IsCustomizedResponse { Result = 0 });
                else
                {
                    var configuration = designConfiguration.Where(x => x.Key == "CustomizedButtonLabel").FirstOrDefault();
                    return Ok(new IsCustomizedResponse { Result = 1, ProductSku = product.Sku, CustomizedButtonLabel = configuration?.Value });
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        private IHttpActionResult HandleResult(ProductDetails productDetails)
        {
            if (productDetails == null)
                return NotFound();
            else
                return Ok(productDetails);
        }

        [Route("api/products/count")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProductsCount(string keyword = "")
        {
            var merchantId = int.Parse(Request.Headers.Authorization.ToString());

            var merchant = MerchantRepository.Get(merchantId);

            var result = await BigCommerceWebClient.GetProductsCountAsync(keyword, merchant.StoreHash, merchant.AccessToken);

            return Ok(new { count = result });
        }
    }
}