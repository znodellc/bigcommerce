﻿using ArtifiConnector.Data;
using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ArtifiConnector.Api
{
    public class CustomerController : ApiController
    {
        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        [Inject]
        public ICustomerRepository CustomerRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        [Route("api/customer")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCustomer(int customerId)
        {
            try
            {
                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var merchant = MerchantRepository.Get(merchantId);
                var content = await Request.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(content);

                var email = jobject.Value<string>("email");
                var bigCommerceCustomerId = await BigCommerceWebClient.GetCustomerIdByEmail(email, merchant.StoreHash, merchant.AccessToken);

                var customer = new Customer { Id = customerId, Email = email, BigCommerceCustomerId = bigCommerceCustomerId };

                CustomerRepository.UpdateCustomer(customer);

                return Ok(new { customerId = customer.Id, email = customer.Email, bigCommerceCustomerId = customer.BigCommerceCustomerId });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }

        [Route("api/customer")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCustomerByEmail(string email = "")
        {
            try
            {
                if (CustomerRepository.CustomerEmailExists(email))
                {
                    var customer = CustomerRepository.GetCustomerByEmail(email);

                    if (customer.BigCommerceCustomerId.HasValue)
                        return Ok(new { customerId = customer.Id, bigCommerceCustomerId = customer.BigCommerceCustomerId, email = customer.Email });
                    else
                        return Ok(new { customerId = customer.Id, email = customer.Email });
                }

                var merchantHash = Request.Headers.Authorization.ToString();
                var merchantId = MerchantRepository.GetMerchantIdByHash(merchantHash);
                var merchant = MerchantRepository.Get(merchantId);
                var bigCommerceCustomerId = await BigCommerceWebClient.GetCustomerIdByEmail(email, merchant.StoreHash, merchant.AccessToken);
                int customerId;

                if (bigCommerceCustomerId.HasValue)
                {
                    var customer = new Customer
                    {
                        BigCommerceCustomerId = bigCommerceCustomerId.Value,
                        Email = email,
                        MerchantId = merchantId
                    };

                    customerId = CustomerRepository.Add(customer);
                    customer = CustomerRepository.GetCustomerByEmail(email);
                    return Ok(new { customerId = customerId, bigCommerceCustomerId = bigCommerceCustomerId, email = customer.Email });
                }
                else
                {
                    customerId = CustomerRepository.AddGuest(merchantId, email);
                    var customer = CustomerRepository.GetCustomerByEmail(email);
                    return Ok(new { customerId = customerId, email = customer.Email });
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return InternalServerError(new Exception(ex.Message));
            }
        }
    }
}
