﻿using ArtifiConnector.Data.Models;
using ArtifiConnector.WebClients.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ArtifiConnector.WebClients
{
    public class BigCommerceWebClient : IBigCommerceWebClient
    {
        private string _clientId;

        public BigCommerceWebClient(string clientId)
        {
            _clientId = clientId;
        }

        public async Task<int> AddModifierAsync(AddModifierRequest request)
        {
            var data = new
            {
                product_id = request.ProductId,
                name = "Artifi Design Id",
                display_name = "Artifi Design Id",
                type = "numbers_only_text",
                config = new { number_integers_only = true },
                required = true
            };

            var jsonRequest = JsonConvert.SerializeObject(data);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", request.AccessToken);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"https://api.bigcommerce.com/stores/{request.StoreHash}/v3/catalog/products/{request.ProductId}/modifiers", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<JObject>("data").Value<int>("id");
            }
        }

        public async Task<int> CreateOption(Option option, string storeHash, string accessToken)
        {
            var data = new
            {
                name = option.Name,
                display_name = option.DisplayName,
                type = "T"
            };

            var jsonRequest = JsonConvert.SerializeObject(data);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/options", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.Created)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<int>("id");
            }
        }

        public async Task<int> CreateOptionSet(string name, string storeHash, string accessToken)
        {
            var data = new
            {
                name = name,
            };

            var jsonRequest = JsonConvert.SerializeObject(data);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/option_sets", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.Created)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<int>("id");
            }
        }

        public async Task<int> CreateOptionSetOption(CreateOptionSetOptionRequest request)
        {
            var data = new
            {
                option_id = request.OptionId,
                display_name = request.DisplayName,
                sort_order = 1,
                is_required = true
            };

            var jsonRequest = JsonConvert.SerializeObject(data);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", request.AccessToken);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PostAsync($"https://api.bigcommerce.com/stores/{request.StoreHash}/v2/option_sets/{request.OptionSetId}/options", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.Created)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                return jobject.Value<int>("id");
            }
        }

        public async Task<int?> GetCustomerIdByEmail(string email, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/customers?email={HttpUtility.UrlEncode(email)}");

                var json = await result.Content.ReadAsStringAsync();



                if (result.StatusCode == HttpStatusCode.NoContent)
                    return null;

                var jobject = JArray.Parse(json).First;

                if (result.StatusCode == HttpStatusCode.BadRequest)
                {
                    if (jobject.Value<string>("message") == "The field 'email' is invalid.")
                        return null;
                }

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return jobject.Value<int>("id");
            }
        }

        public async Task<IEnumerable<int>> GetOptionsForOptionSet(int optionSetId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/option_sets/{optionSetId}/options");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JArray.Parse(json);

                return jobject.Select(x => x.Value<int>("option_id"));
            }
        }

        public async Task<Order> GetOrderAsync(int orderId, string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/orders/{orderId}");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<Order>(json);
            }
        }

        public async Task<IEnumerable<OrderProduct>> GetOrderProductsAsync(int orderId, string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/orders/{orderId}/products");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jarray = JArray.Parse(json);
                var productOptions = new List<OrderProduct>();

                foreach (var item in jarray)
                {
                    var options = item.Value<JArray>("product_options");

                    var orderProduct = new OrderProduct();

                    orderProduct.OrderItemId = item.Value<int>("id");
                    orderProduct.Sku = item.Value<string>("sku");

                    orderProduct.ProductOptions = options.Select(x => new ProductOption
                    {
                        DisplayName = x.Value<string>("display_name"),
                        Value = x.Value<string>("value")
                    });

                    productOptions.Add(orderProduct);
                }

                return productOptions;
            }
        }

        public async Task<int?> GetProductOptionSetId(int productId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products/{productId}");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                var jobject = JObject.Parse(json);

                var optionSetId = jobject.Value<int?>("option_set_id");

                optionSetId = optionSetId == 0 ? null : optionSetId;

                return optionSetId;
            }
        }

        public async Task<IEnumerable<Product>> GetProductsAsync(int page, string keyword, string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products?page={page}&limit=20&keyword_filter={keyword}");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode == HttpStatusCode.NoContent)
                    return null;

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
            }
        }

        public async Task<int> GetProductsCountAsync(string keyword, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products/count?keyword_filter={keyword}");

                var json = await result.Content.ReadAsStringAsync();
                var jobject = JObject.Parse(json);

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return jobject.Value<int>("count");
            }
        }

        public async Task<JObject> GetProductSkusAsync(int productId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products/{productId}/skus");

                if (result.StatusCode == HttpStatusCode.NoContent)
                    return null;

                var json = await result.Content.ReadAsStringAsync();
                var array = JArray.Parse(json);

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return new JObject(
                   from item in array
                   select new JProperty(
                       item.Value<string>("sku"),
                       item.Value<JArray>("options")
                       )
                   );
            }
        }

        public async Task<ReceiveTokenResponse> ReceiveTokenAsync(ReceiveTokenRequest request)
        {
            using (var client = new HttpClient())
            {
                var parts = new[]
                {
                    $"client_id={request.ClientId}",
                    $"client_secret={request.ClientSecret}",
                    $"code={request.Code}",
                    $"scope={request.Scope}",
                    $"grant_type={request.GrantType}",
                    $"redirect_uri={request.RedirectUri}"
                };
                var queryString = string.Join("&", parts);
                var content = new StringContent(queryString, Encoding.UTF8, "application/x-www-form-urlencoded");
                var result = await client.PostAsync("https://login.bigcommerce.com/oauth2/token", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<ReceiveTokenResponse>(json);
            }
        }

        public void RemoveCartItem(int cartItemId)
        {
            throw new NotImplementedException();
        }

        public async Task RemoveModifierAsync(RemoveModifierRequest request)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", request.AccessToken);

                var result = await client.DeleteAsync($"https://api.bigcommerce.com/stores/{request.StoreHash}/v3/catalog/products/{request.ProductId}/modifiers/{request.ModifierId}");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK && result.StatusCode != HttpStatusCode.NoContent)
                    throw new InvalidOperationException(json);
            }
        }

        public async Task RemoveOptionSetFromProduct(int optionSetId, int productId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var jsonRequest = JsonConvert.SerializeObject(new { option_set_id = (int?)null });

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PutAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products/{productId}", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }

        public async Task UpdateOrderStaffNotes(int orderId, string staffNotes, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var jsonRequest = JsonConvert.SerializeObject(new { staff_notes = staffNotes });

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PutAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/orders/{orderId}", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }



        public async Task<bool> OptionSetExists(int optionSetId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/option_sets/{optionSetId}");

                var json = await result.Content.ReadAsStringAsync();

                return result.StatusCode == HttpStatusCode.OK;
            }
        }

        public async Task<bool> OptionExists(int optionId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/options/{optionId}");

                var json = await result.Content.ReadAsStringAsync();

                return result.StatusCode == HttpStatusCode.OK;
            }

        }

        public async Task UpdateProductOptionSet(int optionSetId, int productId, string storeHash, string accessToken)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var jsonRequest = JsonConvert.SerializeObject(new { option_set_id = optionSetId });

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var result = await client.PutAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products/{productId}", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);
            }
        }
    }
}