﻿namespace ArtifiConnector.WebClients.Models
{
    public class RemoveModifierRequest
    {
        public string AccessToken { get; set; }
        public int ModifierId { get; set; }
        public int ProductId { get; set; }
        public string StoreHash { get; set; }
    }
}