﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtifiConnector.Data.Models
{
    public class Order
    {
        public int Id { get; set; }
        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }
    }
}
